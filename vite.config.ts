/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],

  server: {
    proxy: {
      '/api': {
        target: process?.env?.API_HOST,
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, ''),
      },
      '/jupyter': {
        target: process?.env?.JUPYTER_HOST,
        changeOrigin: true,
        rewrite: path => path.replace(/^\/jupyter/, ''),
      },
    },
  },

  build: {
    sourcemap: false,
    rollupOptions: {
      output: {
        manualChunks: {
          react: [
            'react',
            'react-router-dom',
            'react-i18next',
            'react-dom',
          ],
          mui: [
            '@mui/material',
            '@mui/lab',
            '@mui/icons-material',
            '@emotion/react',
            '@emotion/styled',
          ],
          vendor: [
            '@date-io/date-fns',
            'date-fns',
            'i18next',
            'i18next-browser-languagedetector',
            'i18next-http-backend',
            'dompurify',
          ],
        },
      },
    },
  },
});
