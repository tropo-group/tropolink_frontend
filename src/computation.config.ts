export const VERTICAL_MOTION = [
  {
    label: 'Meteorological model’s vertical velocity fields (data)',
    value: '0',
  },
  { label: 'Isobaric (isob)', value: '1' },
  { label: 'Isentropic (isen)', value: '2' },
  { label: 'Constant density (dens)', value: '3' },
  { label: 'Constant internal sigma coordinate (sigma)', value: '4' },
  { label: 'Velocity divergence (diverg)', value: '5' },
  {
    label: 'Vertical coordinate remapping from MSL to AGL (msl2agl)',
    value: '6',
  },
  { label: 'Spatially average the vertical velocity (average)', value: '7' },
];

export const RUNTIME = {
  min: -144,
  max: 144,
 // stepSize: 12,
 stepSize: 1,
  stepMarks: 24,
  marksFixed: 0,
};

export const BUFFER_TYPES = [
  { label: 'Circular buffer', value: 'buffer' },
  { label: 'Geographic buffer', value: 'geographic_buffer' },
];

export const RADIUS_BUFFER = {
  buffer: {
    min: 0,
    max: 100,
    stepSize: 1,
    stepMarks: 20,
    marksFixed: 0,
    prefix: ' km',
  },
  geographic_buffer: {
    min: 0,
    max: 2,
    //max: 90
    stepSize: 0.01,
    //stepSize : 1
    //stepMark:20
    stepMarks: 0.4,
    marksFixed: 1,
    prefix: '°',
  },
};
