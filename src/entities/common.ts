export interface LabelValue<T> {
  label: string;
  value: T;
}

export type Modify<T, R> = Omit<T, keyof R> & R;
