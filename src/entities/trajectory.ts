import { FormError } from '../utils/form';
import { Modify } from './common';
import {
  ComputationId,
  ComputationLocation,
  ComputationStatus,
} from './computation';

export interface Hysplit {
  hour_utc: number;
  runtime: number;
  top_of_model: number;
  vertical_motion: number;
}
export interface TrajectoryModel extends Hysplit {
  name: string;
  description: string;
  slug: string;
  dates: Date[];
  locations: ComputationLocation[];
}
export interface Trajectory extends TrajectoryModel {
  id: ComputationId;
  creation_date: Date;
  study: number;
  status: ComputationStatus;
  jobs: {[key: string]: string};
  jobs_successful: number[];
  jobs_failed: number[];
  nb_traj_points: number;
}

export type TrajectoryRaw = Modify<
Trajectory,
{
  dates: string[];
  locations: string[];
}
>;

export type TrajectoryFormData = Modify<
TrajectoryModel,
{
  dates: string[];
  locations: string[];
}
>;
export interface TrajectoryError {
  name: FormError;
  locations: FormError;
  dates: FormError;
  hour_utc: FormError;
}
