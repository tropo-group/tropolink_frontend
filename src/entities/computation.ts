export interface ComputationLocation {
  value: string
  id: string
  valid: boolean
  selected?: boolean
}

export interface ComputationDate {
  value: Date
  selected?: boolean
}

export type ComputationId = number;

export type ComputationStatus = 'succeeded' | 'failed' | 'in_progress';
