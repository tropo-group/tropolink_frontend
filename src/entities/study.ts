import { ComputationId } from './computation';
import { Connectivity } from './connectivity';
import { Trajectory } from './trajectory';

export interface Study {
  connectivities: Connectivity[];
  creation_date: string;
  modification_date: string;
  id: ComputationId;
  name: string;
  slug: string;
  description: string;
  trajectory: Trajectory;
  user: string;
}
