import { LabelValue, Modify } from './common';
import {
  ComputationDate,
  ComputationId,
  ComputationLocation,
  ComputationStatus,
} from './computation';

export type ConnectivityMethod = 'buffer' | 'geographic_buffer' | string;
export interface ConnectivityConfig {
  method: ConnectivityMethod;
  radius: number;
}

export interface ConnectivityModel {
  method: ConnectivityMethod;
  name: string;
  slug?: string;
  study: ComputationId;
  dates: ComputationDate[];
  locations: ComputationLocation[];
  parameters: {
    config: ConnectivityConfig;
    trajectories_studies: LabelValue<ComputationId>[];
  };
}

export interface Connectivity extends ConnectivityModel {
  id: ComputationId;
  creation_date: string;
  study: number;
  status: ComputationStatus;
  progress: number;
}

export type ConnectivityRaw = Modify<
Connectivity,
{
  dates: string[];
  locations: string[];
}
>;

export type ConnectivityFormData = Modify<
ConnectivityModel,
{
  dates: string[];
  locations: string[];
}
>;
