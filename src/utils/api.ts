import { KeycloakInstance } from 'keycloak-js';
import { TrajectoryFormData, TrajectoryRaw } from '../entities/trajectory';
import { Study } from '../entities/study';
import {
    ConnectivityFormData,
    // Connectivity,
    ConnectivityModel,
    ConnectivityRaw,
} from '../entities/connectivity';
import { ComputationId } from '../entities/computation';

const API_HOST = import.meta.env.VITE_API_HOST;
const MIN_VALIDITY = 300;

export function fetchStudies (
  keycloak: KeycloakInstance,
  signal?,
): Promise<Study[]> {
  return keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/studies/`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
          signal,
        }).then(studies =>
          (studies.ok ? studies.json() : [{ code: studies.status }]));
      } catch (e) {
        // if (e.name === "AbortError") {
        //   console.warn("The fetch has been aborted.");
        //   return null;
        // }

        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });
}

export function fetchTrajectories (
  keycloak: KeycloakInstance,
  signal?,
): Promise<TrajectoryRaw[]> {
  return keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/trajectories/`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
          signal,
        }).then(trajectories =>
          (trajectories.ok
            ? trajectories.json()
            : [{ code: trajectories.status }]));
      } catch (e) {
        // if (e.name === "AbortError") {
        //   console.warn("The fetch has been aborted.");
        //   return null;
        // }
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });
}

export const fetchConnectivity = async (
  keycloak: KeycloakInstance,
  id: ComputationId,
  signal?,
): Promise<ConnectivityRaw> =>
  keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/connectivities/${id}/`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
          signal,
        }).then(trajectory =>
          (trajectory.ok ? trajectory.json() : { code: trajectory.status }));
      } catch (e) {
        // if (e.name === "AbortError") {
        //   // eslint-disable-next-line no-console
        //   console.warn("The fetch has been aborted.");
        //   return null;
        // }
        // eslint-disable-next-line no-console
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });

export const fetchTrajectory = async (
  keycloak: KeycloakInstance,
  id: ComputationId,
  signal?,
): Promise<TrajectoryRaw> =>
  keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/trajectories/${id}/`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
          signal,
        }).then(trajectory =>
          (trajectory.ok ? trajectory.json() : { code: trajectory.status }));
      } catch (e) {
        // if (e.name === "AbortError") {
        //   // eslint-disable-next-line no-console
        //   console.warn("The fetch has been aborted.");
        //   return null;
        // }
        // eslint-disable-next-line no-console
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });

export const deleteStudy = async (
  keycloak: KeycloakInstance,
  id: ComputationId,
) =>
  keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/studies/${id}/`, {
          method: 'DELETE',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
        });
      } catch (e) {
        // if (e.name === "AbortError") {
        //   // eslint-disable-next-line no-console
        //   console.warn("The fetch has been aborted.");
        //   return null;
        // }
        // eslint-disable-next-line no-console
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });

export const deleteConnectivity = async (
  keycloak: KeycloakInstance,
  id: ComputationId,
) =>
  keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/connectivities/${id}/`, {
          method: 'DELETE',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
        });
      } catch (e) {
        // if (e.name === "AbortError") {
        //   // eslint-disable-next-line no-console
        //   console.warn("The fetch has been aborted.");
        //   return null;
        // }
        // eslint-disable-next-line no-console
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });

export const saveTrajectory = async (
  keycloak: KeycloakInstance,
  data: TrajectoryFormData,
) =>
  keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/trajectories/`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
          body: JSON.stringify(data),
        }).then(trajectories =>
          (trajectories.ok ? trajectories.json() : { code: trajectories.status }));
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });

export const patchStudy = async (keycloak, { id = null, ...changes } = {}) =>
  id && keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/studies/${id}/`, {
          method: 'PATCH',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
          body: JSON.stringify(changes),
        }).then(trajectories =>
          (trajectories.ok ? trajectories.json() : { code: trajectories.status }));
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });

export const saveConnectivity = async (
  keycloak: KeycloakInstance,
  data: ConnectivityFormData,
) =>
  keycloak
    .updateToken(MIN_VALIDITY)
    .then(async () => {
      try {
        return await fetch(`${API_HOST}/connectivities/`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloak.token}`,
          },
          body: JSON.stringify(data),
        }).then(connectivity =>
          (connectivity.ok ? connectivity.json() : { code: connectivity.status }));
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
        return null;
      }
    })
    .catch(() => {
      keycloak.clearToken();
      return null;
    });
