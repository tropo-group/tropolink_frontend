export function handleFile (file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.onloadend = () => {
      if (typeof fileReader.result === 'string') {
        resolve(fileReader.result);
      }
      reject();
    };
    fileReader.readAsText(file);
  });
}

export function handleJSONArrayFile (file: File): Promise<string[]> {
  return handleFile(file).then(result => {
    try {
      const parseResult = JSON.parse(result);
      if (Array.isArray(parseResult)) {
        return parseResult;
      }
    } catch (e) {
      return Promise.reject();
    }

    return Promise.reject();
  });
}

export function handleJSONFile (file: File): Promise<unknown> {
  return handleFile(file).then(result => {
    const parseResult = JSON.parse(result);
    return parseResult;
  });
}
