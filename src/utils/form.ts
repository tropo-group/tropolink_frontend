import { Mark } from '@mui/material';

export const validateRequired = value => !!value;
export const validateIsNumber = value => !!+value;
export const validateMinLength = (value, length) =>
  (value ? value.length >= length : false);
export const validateMaxLength = (value, length) =>
  (value ? value.length <= length : false);
export const validateMinNumber = (value, min) =>
  (value === 0 || value ? +value >= min : false);
export const validateMaxNumber = (value, max) =>
  (value === 0 || value ? +value <= max : false);

export enum FormError {
  Required = 'required',
  IsNumber = 'number',
  Length = 'length',
  Min = 'min',
  Max = 'max',
  Same = 'same',
  Pattern = 'pattern',
}

export function getErrorMessage (error?: FormError) {
  switch (error) {
    case FormError.Required:
      return 'This field is required.';

    case FormError.Same:
      return 'This study already existed.';

    case FormError.IsNumber:
      return 'This field should be a number.';

    case FormError.Length:
      return 'This field should contain between 3 and 30 characters';

    default:
      return '';
  }
}

export interface MarkConfig {
  min: number;
  max: number;
  stepSize: number;
  stepMarks: number;
  marksFixed: number,
  prefix?: string;
}

export function getSliderMarks ({
  min,
  max,
  stepMarks,
  marksFixed,
  prefix,
}: MarkConfig): Mark[] {
  const marks: Mark[] = [];
  while (marks.length <= (max - min) / stepMarks) {
    const previousValue = marks[marks.length - 1];
    const value =      previousValue !== undefined ? previousValue.value + stepMarks : min;

    marks.push({
      value,
      label: value.toFixed(marksFixed) + (prefix || ''),
    });
  }
  return marks;
}
