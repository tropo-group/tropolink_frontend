import { v4 as uuidv4 } from 'uuid';
import { ComputationLocation } from '../entities/computation';

function valideLocations (
  locations: ComputationLocation[],
): ComputationLocation[] {
  return locations.map(loc => {
    const {
      // 0: code,
      // 1: name,
      2: latitude,
      3: longitude,
      4: height,
    } = loc.value.trim().split(/\s+/);

    let valid = true;

    if ([latitude, longitude, height].some(value => !Number.isFinite(Number(value)))) {
      valid = false;
    }

    if (typeof height === 'undefined') {
      valid = false;
    }

    return { ...loc, valid };
  });
}

export function parseLocations (array: any[]): ComputationLocation[] {
  if (array.every(str => typeof str === 'string')) {
    const locations = array
      .filter(Boolean)
      .map(loc => ({
        value: loc,
        id: uuidv4(),
        valid: true,
      }));

    return valideLocations(locations);
  }

  return valideLocations(array);
}

export function stringifyLocations (locations: ComputationLocation[]): string[] {
  return locations.map(({ value }) => {
    const splitLocs = value.split(/\s+/);
    return [
      splitLocs[0],
      splitLocs[1],
      splitLocs[2],
      splitLocs[3],
      splitLocs[4],
    ].join(' ');
  });
}
