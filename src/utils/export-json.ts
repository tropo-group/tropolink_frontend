export default function exportJSON (name, json) {
  const fileName = `${name}.json`;

  // Create a blob of the data
  const file = new Blob([JSON.stringify(json)], {
    type: 'application/json',
  });

  const a = document.createElement('a');
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
}
