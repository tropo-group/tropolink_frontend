import {
  Connectivity,
  ConnectivityFormData,
  ConnectivityModel,
  ConnectivityRaw,
} from '../entities/connectivity';
import { Trajectory } from '../entities/trajectory';
import { parseLocations, stringifyLocations } from './locations';
import {formatDate, parseDates, stringifyDates} from "./dates";

export function getConnectivityModelFromTrajectory (
  trajectory: Trajectory,
): ConnectivityModel {
  const failedDates = trajectory.jobs_failed.map((job) => trajectory.jobs[job]);

  return {
    method: 'buffer',
    name: '',
    dates: trajectory.dates
        .filter((date) => failedDates.indexOf(formatDate(date)) === -1)
        .map((date) => ({value: date, selected: true})),
    locations: trajectory.locations.map((location) => ({...location, selected: true})),
    parameters: {
      config: {
        method: 'buffer',
        radius: 10,
      },
      trajectories_studies: [
        {
          label: trajectory.name,
          value: trajectory.id,
        },
      ],
    },
    study: trajectory.study,
  };
}

export function connectivityFormData (
  connectivityModel: ConnectivityModel,
): ConnectivityFormData {
  return {
    ...connectivityModel,
    dates: stringifyDates(connectivityModel.dates.filter(date => !!date.selected)),
    locations: stringifyLocations(connectivityModel.locations.filter(location => !!location.selected)),
  };
}

export function connectivityRawData (
  connectivity: Connectivity,
): ConnectivityRaw {
  return {
    ...connectivity,
    dates: stringifyDates(connectivity.dates),
    locations: stringifyLocations(connectivity.locations),
  };
}

export function parseConnectivity (
  connectivityRaw: ConnectivityRaw,
): Connectivity {
  return {
    ...connectivityRaw,
    dates: parseDates(connectivityRaw.dates),
    locations: parseLocations(connectivityRaw.locations),
    parameters: {
      ...connectivityRaw.parameters,
    },
  };
}
