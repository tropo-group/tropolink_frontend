import formatISO from 'date-fns/formatISO';
import {ComputationDate} from "../entities/computation";
import {stringifyLocations} from "./locations";

export type DatePickerDate = any | Date | number | string;

export const convertUTCDateToLocalDate = date => {
  const newDate = new Date(0);
  newDate.setUTCFullYear(date.getFullYear(), date.getMonth(), date.getDate());
  return newDate;
};

export function formatDate (date: Date): string {
  return date.toISOString().slice(0, 10);
  // return date.toLocaleDateString('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' });
}

export function parseDate (str): Date {
  return new Date(str);
}

export function parseDates (array): ComputationDate[] {
  return array.map(str => ({value: new Date(str)}));
}

export function stringifyDates (dates: ComputationDate[]): string[] {
  return dates.map(date => formatDate(date.value));
}

export function getDates (rangeDate: [DatePickerDate, DatePickerDate]): Date[] {
  const dates: Date[] = [];
  const currentDate = new Date(rangeDate[0]);
  while (currentDate <= rangeDate[1]) {
    dates.push(new Date(currentDate));
    currentDate.setTime(currentDate.getTime() + (
      24 * 3600 * 1000
    ));
  }
  return dates;
}

export const sortDates = dates => dates.sort((a, b) => a.getTime() - b.getTime());

export const uniqueDates = dates => dates.filter(
  (date, i, localDates) => localDates
    .map(localDate => localDate.getTime())
    .indexOf(date.getTime()) === i,
);

export const isoDate = date => formatISO(date, { representation: 'date' });
