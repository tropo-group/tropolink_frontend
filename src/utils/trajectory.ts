import {
  Trajectory,
  TrajectoryError,
  TrajectoryFormData,
  TrajectoryModel,
  TrajectoryRaw,
} from '../entities/trajectory';
import { parseDate } from './dates';
import {
  FormError,
  validateMinLength,
  validateMaxLength,
  validateMaxNumber,
  validateMinNumber,
  validateRequired,
} from './form';
import { parseLocations, stringifyLocations } from './locations';

export function parseTrajectory (trajectoryRaw: TrajectoryRaw): Trajectory {
  return {
    ...trajectoryRaw,
    locations: parseLocations(trajectoryRaw.locations),
    dates: trajectoryRaw.dates.map(date => parseDate(date)),
  };
}

export function trajectoryFormData (
  trajectory: TrajectoryModel,
): TrajectoryFormData {
  return {
    ...trajectory,
    dates: trajectory.dates.map(date => date.toISOString().slice(0, 10)),
    locations: stringifyLocations(trajectory.locations),
  };
}

export function getDataFromTrajectory (trajectory: Trajectory): TrajectoryModel {
  return {
    name: trajectory.name,
    slug: trajectory.slug,
    description: trajectory.description,
    dates: trajectory.dates,
    hour_utc: trajectory.hour_utc,
    locations: trajectory.locations,
    runtime: trajectory.runtime,
    top_of_model: trajectory.top_of_model,
    vertical_motion: trajectory.vertical_motion,
  };
}

export function validateComputeTrajectory (
  trajectories: Trajectory[],
  trajectory: TrajectoryModel,
): Partial<TrajectoryError> {
  const errors: Partial<TrajectoryError> = {};

  if (trajectory.locations.some(({ valid }) => valid === false)) {
    errors.locations = FormError.Pattern;
  } else if (trajectory.locations.length < 1) {
    errors.locations = FormError.Required;
  }

  if (!validateRequired(trajectory.name)) {
    errors.name = FormError.Required;
  } else if (!validateMinLength(trajectory.name, 3) || !validateMaxLength(trajectory.name, 30)) {
    errors.name = FormError.Length;
  }

  if (!validateMinLength(trajectory.dates, 1)) {
    errors.dates = FormError.Required;
  }

  const slugify = text =>
    text
      .normalize('NFKD')
      .toLowerCase()
      .replace(/[^\w\s-]/g, '')
      .trim()
      .replace(/[-\s]+/g, '-')
      .replace(/^[-_]+/g, '')
      .replace(/[-_]+$/g, '');

  if (trajectories.length > 0) {
    if (trajectories.some(({ name }) => slugify(name) === slugify(trajectory.name))) {
      errors.name = FormError.Same;
    }
  }
/** HR modif des bornes
  if (!validateMaxNumber(trajectory.hour_utc, 24)) {
    errors.hour_utc = FormError.Max;
  }
 if (!validateMinNumber(trajectory.hour_utc, 1)) {
    errors.hour_utc = FormError.Min;
  }

 **/
    if (!validateMaxNumber(trajectory.hour_utc, 23)) {
    errors.hour_utc = FormError.Max;
  }

  if (!validateMinNumber(trajectory.hour_utc, 0)) {
    errors.hour_utc = FormError.Min;
  }

  return errors;
}
