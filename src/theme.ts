import { grey, red } from '@mui/material/colors';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

// A custom theme for this app
const theme = createTheme({
  palette: {
    primary: {
      main: '#0099ff',
    },
    secondary: {
      main: '#009688',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: grey[100],
    },
  },
  typography: {
    h1: { fontSize: '3rem' },
    h2: { fontSize: '2.5rem' },
    h3: { fontSize: '1.75rem' },
    h4: { fontSize: '1.25rem' },
    h5: { fontSize: '1rem' },
    h6: { fontSize: '0.875rem' },
  },
});

export default responsiveFontSizes(theme);
