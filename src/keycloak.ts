import Keycloak from 'keycloak-js';

// console.log('keycloak', import.meta.env.VITE_KEYCLOAK_URL);

const keycloak = Keycloak({
  realm: 'tropolink', // realm as configured in Keycloak
  url: import.meta.env.VITE_KEYCLOAK_URL, // URL of the Keycloak server
  clientId: 'tropolink-api-front', // client id as configured in the realm in Keycloak
});

export default keycloak;
