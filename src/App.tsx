import DateFnsAdapter from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { ReactKeycloakProvider } from '@react-keycloak/web';
import { SnackbarProvider } from 'notistack';
import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { Container } from '@mui/material';

import Layout from './components/Layout';
import keycloak from './keycloak';
import Analyse from './pages/Analyse';
import ComputeConnectivity from './pages/ComputeConnectivity';
import ComputeTrajectory from './pages/ComputeTrajectory';
import DetailConnectivity from './pages/DetailConnectivity';
import DetailTrajectory from './pages/DetailTrajectory';
import Loading from './components/Loading';
import MarkdownContent from './components/MarkdownContent';
import StudyList from './pages/List';
import './styles.css';

const App = () => (
  <LocalizationProvider dateAdapter={DateFnsAdapter}>
    <SnackbarProvider maxSnack={3}>
      <React.Suspense fallback={<Loading />}>
        <Layout>
          <ReactKeycloakProvider
            authClient={keycloak}
            initOptions={{
              onLoad: 'login-required',
              checkLoginIframe: false,
            }}
            LoadingComponent={<Loading />}
          >
            <Routes>
              <Route path="/" element={<Navigate to="/list" replace />} />
              <Route path="/list" element={<StudyList />} />
              <Route path="/trajectory/:id" element={<DetailTrajectory />} />
              <Route
                path="/connectivity/:id"
                element={<DetailConnectivity />}
              />
              <Route
                path="/compute-trajectory"
                element={<ComputeTrajectory />}
              />
              <Route
                path="/compute-connectivity"
                element={<ComputeConnectivity />}
              />
              <Route path="/analyse" element={<Navigate to="/" replace />} />
              <Route path="/analyse/:language" element={<Analyse />} />
              <Route
                path="/credit"
                element={(
                  <Container>
                    <MarkdownContent fileName="credit.md" />
                  </Container>
                  )}
              />
              <Route
                path="/help"
                element={(
                  <Container>
                    <MarkdownContent fileName="help.md" />
                  </Container>
                  )}
              />
            </Routes>
          </ReactKeycloakProvider>
        </Layout>
      </React.Suspense>
    </SnackbarProvider>
  </LocalizationProvider>
);

export default App;
