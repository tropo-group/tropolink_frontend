import React from 'react';
import { useKeycloak } from '@react-keycloak/web';
import { useQuery } from '@tanstack/react-query';

import { fetchTrajectory } from '../utils/api';
import { parseTrajectory } from '../utils/trajectory';

const useTrajectory = id => {
  const [pending, setPending] = React.useState<boolean>(true);
  const { keycloak } = useKeycloak();

  const trajectoryQueryResponse = useQuery(
    ['trajectory', id],
    () => fetchTrajectory(keycloak, +id),
    { enabled: pending, staleTime: 3000, refetchInterval: 10000, },
  );

  React.useEffect(
    () => {
      const isDone = ['succeeded', 'failed'].includes(trajectoryQueryResponse?.data?.status || '');
      if (isDone) {
        setPending(false);
      }
    },
    [trajectoryQueryResponse?.data?.status],
  );

  return React.useMemo(
    () => ({
      ...trajectoryQueryResponse,
      trajectory: trajectoryQueryResponse.data
        ? parseTrajectory(trajectoryQueryResponse.data)
        : null,
    }),
    [trajectoryQueryResponse],
  );
};

export default useTrajectory;
