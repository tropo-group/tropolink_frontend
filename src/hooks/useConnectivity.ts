import React from 'react';
import { useKeycloak } from '@react-keycloak/web';
import { useQuery } from '@tanstack/react-query';

import { fetchConnectivity } from '../utils/api';
import { parseConnectivity } from '../utils/connectivity';

const useConnectivity = id => {
  const [pending, setPending] = React.useState<boolean>(true);
  const { keycloak } = useKeycloak();

  const connectivityQueryResponse = useQuery(
    ['connectivity', id],
    () => fetchConnectivity(keycloak, +id),
    { enabled: pending, staleTime: 3000, refetchInterval: 10000, },
  );

  React.useEffect(
    () => {
      const isDone = ['succeeded', 'failed'].includes(connectivityQueryResponse?.data?.status || '');
      if (isDone) {
        setPending(false);
      }
    },
    [connectivityQueryResponse?.data?.status],
  );

  return React.useMemo(
    () => ({
      ...connectivityQueryResponse,
      connectivity: connectivityQueryResponse.data
        ? parseConnectivity(connectivityQueryResponse.data)
        : null,
    }),
    [connectivityQueryResponse],
  );
};

export default useConnectivity;
