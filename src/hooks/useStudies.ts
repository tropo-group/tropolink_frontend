import React from 'react';
import { useKeycloak } from '@react-keycloak/web';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';

import { useTranslation } from 'react-i18next';
import { useSnackbar } from 'notistack';

import { deleteConnectivity, deleteStudy, patchStudy, fetchStudies } from '../utils/api';

const useStudies = () => {
  const { t } = useTranslation();
  const { keycloak } = useKeycloak();
  const queryClient = useQueryClient();
  const { enqueueSnackbar } = useSnackbar();

  const displaySuccess = React.useCallback(
    str => enqueueSnackbar(str, {
      variant: 'success',
      anchorOrigin: { horizontal: 'center', vertical: 'top' },
    }),
    [enqueueSnackbar],
  );

  const displayDeletitionSuccess = React.useCallback(
    () => displaySuccess(t('The study has been deleted.')),
    [displaySuccess, t],
  );

  const displayError = React.useCallback(
    () => enqueueSnackbar(t('This action has encountered an unexpected error.'), {
      variant: 'error',
      anchorOrigin: { horizontal: 'center', vertical: 'top' },
    }),
    [enqueueSnackbar, t],
  );

  const studiesQueryResponse = useQuery(['studies'], () => fetchStudies(keycloak), {
    staleTime: 3000,
    refetchInterval: 10000,
  });

  const handleMutationError = React.useCallback(
    (err, newTodo, context) => {
      displayError();
      queryClient.setQueryData(
        ['studies'],
        context?.previousStudies,
      );
    },
    [displayError, queryClient],
  );

  const deleteStudyMutation = useMutation({
    mutationFn: id => deleteStudy(keycloak, id),

    // When mutate is called:
    onMutate: async id => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries({ queryKey: ['studies'] });

      // Snapshot the previous value
      const previousStudies = queryClient.getQueryData(['studies']);

      // Optimistically update to the new value
      queryClient.setQueryData(
        ['studies'],
        (prevStudies: Array<any> = []) => prevStudies.filter(({ id: itemId }) => itemId !== id),
      );

      // Return a context object with the snapshotted value
      return { previousStudies };
    },

    // If the mutation fails, use the context returned from onMutate to roll back
    onError: handleMutationError,

    // Always refetch after error or success:
    onSettled: () => {
      displayDeletitionSuccess();
      queryClient.invalidateQueries({ queryKey: ['studies'] });
    },
  });

  const patchStudyMutation = useMutation({
    mutationFn: changes => patchStudy(keycloak, changes),

    // When mutate is called:
    onMutate: async ({ id, ...changes }) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries({ queryKey: ['studies'] });

      // Snapshot the previous value
      const previousStudies = queryClient.getQueryData(['studies']);

      // Optimistically update to the new value
      queryClient.setQueryData(
        ['studies'],
        (prevStudies: Array<any> = []) => prevStudies.map(study => (
          study.id === id
            ? { ...study, ...changes }
            : study
        )),
      );

      // Return a context object with the snapshotted value
      return { previousStudies };
    },

    // If the mutation fails, use the context returned from onMutate to roll back
    onError: handleMutationError,

    // Always refetch after error or success:
    onSettled: () => {
      displaySuccess(t('Study has been updated'));
      queryClient.invalidateQueries({ queryKey: ['studies'] });
    },
  });

  const deleteConnectivityMutation = useMutation({
    mutationFn: id => deleteConnectivity(keycloak, id),

    // When mutate is called:
    onMutate: async id => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries({ queryKey: ['studies'] });

      // Snapshot the previous value
      const previousStudies = queryClient.getQueryData(['studies']);

      // Optimistically update to the new value
      queryClient.setQueryData(
        ['studies'],
        (prevStudies: Array<any> = []) => prevStudies.map(({ connectivities = [], ...study }) => ({
          ...study,
          connectivities: connectivities.filter(
            ({ id: connectivityId }) => connectivityId !== id,
          ),
        })),
      );

      // Return a context object with the snapshotted value
      return { previousStudies };
    },

    // If the mutation fails, use the context returned from onMutate to roll back
    onError: handleMutationError,

    // Always refetch after error or success:
    onSettled: () => {
      displayDeletitionSuccess();
      queryClient.invalidateQueries({ queryKey: ['studies'] });
    },
  });

  return {
    ...studiesQueryResponse,
    studies: studiesQueryResponse.data,
    deleteStudy: deleteStudyMutation.mutateAsync,
    patchStudy: patchStudyMutation.mutateAsync,
    deleteConnectivity: deleteConnectivityMutation.mutateAsync,
  };
};

export default useStudies;
