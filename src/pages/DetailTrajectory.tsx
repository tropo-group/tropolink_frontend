import React, { useState } from 'react';

import {
  Alert,
  Box,
  Button, Chip,
  Container,
  Divider,
  Grid,
  IconButton,
  Paper, Snackbar,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { ContentCopy, Download as DownloadIcon } from '@mui/icons-material';

import { useKeycloak } from '@react-keycloak/web';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import Map from '../components/Map';
import {formatDate} from '../utils/dates';
import exportJSON from '../utils/export-json';
import ListLocations from '../components/ListLocations';
import { VERTICAL_MOTION } from '../computation.config';
import StatusChip from '../components/StatusChip';
import useTrajectory from '../hooks/useTrajectory';
import TrajectoryProgress from "../components/TrajectoryProgress";
import {useSnackbar} from "notistack";

export const DetailTrajectory = () => {
  const { t } = useTranslation();
  const { keycloak } = useKeycloak();
  const [busyResultsDataQuery, setBusyResultsDataQuery] = useState<boolean>(false);
  const [busyLogsDataQuery, setBusyLogsDataQuery] = useState<boolean>(false);
  const { enqueueSnackbar } = useSnackbar();

  const { id } = useParams();
  const { isLoading, trajectory } = useTrajectory(id);

  const points = trajectory?.locations.map(location => {
    const locationData = location.value.split(' ');
    return [locationData[2], locationData[3]];
  }) || [];

  const datesInErrors = trajectory?.jobs_failed?.map(job => {
    return trajectory.jobs[job];
  }) || [];

  // TODO: style loading
  if (isLoading) {
    return <p>loading</p>;
  }

  const copy = text => {
    navigator.clipboard.writeText(text);
    // TODO: snackbar
    // toast.displayToaster("ok", t("study.copy-success"))
  };

  const handleExport = () => {
    if (trajectory !== null) {
      // Create a blob of the data
      const json = {
        creation_date: trajectory.creation_date,
        hour_utc: trajectory.hour_utc,
        runtime: trajectory.runtime,
        vertical_motion: trajectory.vertical_motion,
        top_of_model: trajectory.top_of_model,
        dates: trajectory.dates,
        locations: trajectory.locations,
        name: trajectory.name,
      };

      exportJSON(trajectory.name, json);
    }
  };

  if (!trajectory) {
    return null;
  }

  const downloadResults = async () => {
    setBusyResultsDataQuery(true);

    const res = await fetch(
      `${import.meta.env.VITE_API_HOST}/trajectories/${id}/get_tdumps/`,
      {
        headers: {
          Authorization: `Bearer ${keycloak.token}`,
        },
      },
    );

    if (res.status !== 200) {
      enqueueSnackbar(t("There was an error."), {
        variant: 'error',
        anchorOrigin: { horizontal: 'center', vertical: 'top' },
      });
      setBusyResultsDataQuery(false);
      return ;
    }
    const json = await res.json();
    window.open(`${import.meta.env.VITE_API_HOST}${json.url}`, '_blank');
    setBusyResultsDataQuery(false);
  };

  const downloadLogs = async () => {
    setBusyLogsDataQuery(true);

    const res = await fetch(
      `${import.meta.env.VITE_API_HOST}/trajectories/${id}/get_logs/`,
      {
        headers: {
          Authorization: `Bearer ${keycloak.token}`,
        },
      },
    );

    if (res.status !== 200) {
      enqueueSnackbar(t("There was an error."), {
        variant: 'error',
        anchorOrigin: { horizontal: 'center', vertical: 'top' },
      });
      setBusyLogsDataQuery(false);
      return ;
    }
    const json = await res.json();
    console.log(`${import.meta.env.VITE_API_HOST}${json.url}`);
    window.open(`${import.meta.env.VITE_API_HOST}${json.url}`, '_blank');
    setBusyLogsDataQuery(false);
  };

  return (
    <Container sx={{ pb: 4 }}>
      <Grid container alignItems="center" justifyContent="space-between">
        <Grid item>
          <Typography variant="h1" paragraph>
            {t('Trajectory')} {trajectory.name}
            {' '}
            <StatusChip status={trajectory.status} component="span" />
          </Typography>
        </Grid>
        <Grid item>
          <Button
            variant="outlined"
            onClick={handleExport}
            endIcon={<DownloadIcon />}
          >
            {t('Download config')}
          </Button>

          <Button
            variant="outlined"
            endIcon={<DownloadIcon />}
            disabled={trajectory.status !== 'succeeded'}
            color="secondary"
            sx={{ ml: 1 }}
            href="#download"
          >
            {t('Download results')}
          </Button>
        </Grid>
      </Grid>

      <Typography variant="h3" gutterBottom sx={{ mb: 4 }}>
        {t('Progress')}
        <TrajectoryProgress trajectory={trajectory} />
      </Typography>

      <Typography variant="h3" gutterBottom>
        {t('Starting/arrival dates')}
        {' '}
        <IconButton
          onClick={() => copy(JSON.stringify(trajectory.dates))}
          aria-label="Copy dates"
          size="small"
        >
          <ContentCopy />
        </IconButton>
      </Typography>

      <Box>
        <Paper sx={{ px: 3, pt: 3, pb: 2 }}>
          {trajectory.status === 'succeeded' && (
              <Box sx={{ mb: 3 }}>Number of dates for which the trajectories could be calculated : {trajectory.jobs_successful.length} / {Object.keys(trajectory.jobs).length}</Box>
           )}
          {trajectory.status === 'failed' && (
              <Box sx={{ mb: 3 }}>There is no dates for which the trajectories could be calculated.</Box>
           )}
          {trajectory.dates.map(date => (
            <Chip
              size="small"
              key={date.getTime()}
              sx={{ mr: 1, mb: 1 }}
              label={formatDate(date)}
              color={datesInErrors.includes(formatDate(date)) ? "error" : "default" }
              variant={datesInErrors.includes(formatDate(date)) ? "outlined" : "filled" }
            />
          ))}
        </Paper>
      </Box>

      <Typography variant="h3" gutterBottom sx={{ mt: 4 }}>
        {t('Starting/arrival locations')}
      </Typography>

      <Map points={points} />

      <ListLocations
        // @ts-ignore
        sx={{ mt: 1 }}
        locations={trajectory.locations}
        afterTitle={(
          <IconButton
            onClick={() => copy(JSON.stringify(trajectory.locations))}
            aria-label="Copy locations"
            size="small"
          >
            <ContentCopy />
          </IconButton>
        )}
      />

      <Typography variant="h3" gutterBottom sx={{ mt: 4 }}>
        {t('Tune HYSPLIT')}
        {/* HYSPLIT tuning ? */}
      </Typography>

      <TableContainer component={Paper} sx={{ display: 'inline-block', width: 'auto', px: 2, py: 1 }}>
        <Table size="small" sx={{ width: 'auto' }}>
          <TableBody>
            <TableRow>
              <TableCell
                component="th"
                variant="head"
              >
                {t('Trajectory duration (h)')}
              </TableCell>
              <TableCell>{trajectory.runtime}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell
                component="th"
                variant="head"
              >
                {t('Starting/Arrival hour (UTC)')}
              </TableCell>
              <TableCell>{trajectory.hour_utc} h</TableCell>
            </TableRow>
            <TableRow>
              <TableCell
                component="th"
                variant="head"
              >
                {t('Top of model domain (m)')}
              </TableCell>
              <TableCell>{trajectory.top_of_model}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell
                component="th"
                variant="head"
              >
                {t('Method')}
              </TableCell>
              <TableCell>{VERTICAL_MOTION?.[trajectory.vertical_motion]?.label}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>

      {trajectory.status !== 'in_progress' && (
        <>
          <Divider sx={{ m: 4 }} />

          <Typography variant="h3" gutterBottom sx={{ mt: 4 }} id="download">
            {t('Download results')}
          </Typography>

          <Paper sx={{ px: 2, py: 1 }}>
            <Box sx={{ mt: 1, mb: 2 }}>
              Warning: if your study is made up of many points/dates, it will take some time to generate the
              archive before you can download it.<br />
              If you get a "timeout" during the operation, you may
              have to wait about an hour before the archive is ready to download.
            </Box>
            <Box>
              {trajectory.status === 'succeeded' && (
                <>
                  <LoadingButton
                    loading={busyResultsDataQuery}
                    variant="outlined"
                    onClick={() => downloadResults()}
                    sx={{ textTransform: 'none' }}
                  >
                    {t('Download all results')}
                  </LoadingButton>
                  {' '}
                </>
              )}
              <LoadingButton
                loading={busyLogsDataQuery}
                variant="outlined"
                onClick={() => downloadLogs()}
                sx={{ textTransform: 'none' }}
              >
                {t('Download all logs')}
              </LoadingButton>
            </Box>
          </Paper>
        </>
      )}
    </Container>
  );
};

export default DetailTrajectory;
