import React, { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Container,
  FormControl,
  MenuItem,
  Paper,
  Select,
  Slider,
  Typography,
} from '@mui/material';
import { useKeycloak } from '@react-keycloak/web';
import { useSnackbar } from 'notistack';
import { useTranslation, Trans } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

import ListLocations from '../../components/ListLocations';
import { BUFFER_TYPES, RADIUS_BUFFER } from '../../computation.config';
import { ComputationId } from '../../entities/computation';
import {ConnectivityFormData, ConnectivityModel, ConnectivityRaw} from '../../entities/connectivity';
import {Trajectory, TrajectoryRaw} from '../../entities/trajectory';
import { fetchTrajectories, saveConnectivity } from '../../utils/api';
import { getSliderMarks } from '../../utils/form';
import {parseTrajectory} from '../../utils/trajectory';
import {connectivityFormData, getConnectivityModelFromTrajectory, parseConnectivity} from '../../utils/connectivity';
import HelpTooltip from '../../components/HelpTooltip';
import ListDates from "../../components/ListDates";
import {handleJSONFile} from "../../utils/file";
import {formatDate} from "../../utils/dates";

const ComputeConnectivity = () => {
  const { t } = useTranslation();
  const { keycloak } = useKeycloak();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const [trajectories, setTrajectories] = useState<Trajectory[]>([]);
  const [trajectoryId, setTrajectoryId] = useState<ComputationId | ''>('');
  const [connectivity, setConnectivity] = useState<ConnectivityModel | null>(
    null,
  );
  const [connectivityFile, setConnectivityFile] = useState<File | null>(null);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    let mounted = true;

    function parseTrajectories (response: any[]): Trajectory[] {
      return response.map(parseTrajectory);
    }

    const getTrajectories = async (isMounted, signal) => {
      if (isMounted) {
        const response = await fetchTrajectories(keycloak, signal);

        setLoading(true);
        // TODO: manage error
        if (response && response.length > 0) {
          setTrajectories(parseTrajectories(response));
        }
      }
    };

    /** Allow to abort fetch if component is being unmounted */
    const abortController = new AbortController();
    getTrajectories(mounted, abortController.signal);

    return () => {
      mounted = false;
      abortController.abort();
    };
  }, [keycloak]);

  function handleSelectTrajectory (e) {
    const id = e.target.value;
    if (typeof id === 'number') {
      setTrajectoryId(id);
    }

    const trajectory = trajectories.find(traj => traj.id === id);

    if (trajectory) {
      setConnectivity(getConnectivityModelFromTrajectory(trajectory));
      setConnectivityFile(null);
    }
  }

  const handleFileread = e => {
    if (e.target.files?.length && connectivity !== null) {
      const file = e.target.files[0];
      handleJSONFile(file).then(data => {
        try {
          if ((data as ConnectivityRaw).parameters.trajectories_studies[0].value !== trajectoryId) {
            enqueueSnackbar(t("This is a connectivity configuration file for another trajectory."), {
              variant: 'error',
              anchorOrigin: { horizontal: 'center', vertical: 'top' },
            });
            return ;
          }
          setConnectivityFile(file);
          setConnectivity({
            ...connectivity,
            method: (data as ConnectivityRaw).method,
            dates: connectivity.dates.map(date => ({
              ...date, selected: (data as ConnectivityRaw).dates.indexOf(formatDate(date.value)) !== -1
            })),
            locations: connectivity.locations.map(location => ({
              ...location, selected: (data as ConnectivityRaw).locations.indexOf(location.value) !== -1
            })),
            parameters: {
              ...(data as ConnectivityRaw).parameters,
            },
          });
        } catch (err) {
          console.error(err);
          console.warn(t('form.error', { context: 'json' }));
          enqueueSnackbar(t("This file doesn't contain a valid json."), {
            variant: 'error',
            anchorOrigin: { horizontal: 'center', vertical: 'top' },
          });
        }
      });
    }
  };

  function handleChangeRadius (e) {
    if (connectivity && e.target.value) {
      setConnectivity({
        ...connectivity,
        parameters: {
          ...connectivity.parameters,
          config: {
            ...connectivity.parameters.config,
            radius: e.target.value,
          },
        },
      });
    }
  }

  function handleChangeBuffer (e) {
    if (connectivity && e.target.value) {
      setConnectivity({
        ...connectivity,
        method: e.target.value,
        parameters: {
          ...connectivity.parameters,
          config: {
            ...connectivity.parameters.config,
            method: e.target.value,
          },
        },
      });
    }
  }

  function toggleLocation (id) {
    if (connectivity) {
      setConnectivity({
        ...connectivity,
        locations: connectivity.locations.map(location => ({
          ...location, selected: location.id === id ? !location.selected: location.selected
        })),
      });
    }
  }

  function selectAllLocations() {
    if (connectivity) {
      setConnectivity({
        ...connectivity,
        locations: connectivity.locations.map(location => ({
          ...location, selected: true
        })),
      });
    }
  }

  function deselectAllLocations() {
    if (connectivity) {
      setConnectivity({
        ...connectivity,
        locations: connectivity.locations.map(location => ({
          ...location, selected: false
        })),
      });
    }
  }

  function toggleDate (value) {
    if (connectivity) {
      setConnectivity({
        ...connectivity,
        dates: connectivity.dates.map(date => ({
          ...date, selected: date.value === value ? !date.selected: date.selected
        })),
      });
    }
  }

  function selectAllDates() {
    if (connectivity) {
      setConnectivity({
        ...connectivity,
        dates: connectivity.dates.map(date => ({
          ...date, selected: true
        })),
      });
    }
  }

  function deselectAllDates() {
    if (connectivity) {
      setConnectivity({
        ...connectivity,
        dates: connectivity.dates.map(date => ({
          ...date, selected: false
        })),
      });
    }
  }

  async function submit () {
    if (!connectivity || connectivity.locations.length === 0 || connectivity.dates.length === 0) {
      return ;
    }

    setLoading(true);

    const formData: ConnectivityFormData = connectivityFormData(connectivity);

    if (connectivity) {
      await saveConnectivity(keycloak, formData);
      setLoading(false);

      enqueueSnackbar(t('The connectivity has been created.'), {
        variant: 'success',
        anchorOrigin: { horizontal: 'center', vertical: 'top' },
      });

      navigate('/list');
    }
  }

  return (
    <Container sx={{ pt: 4 }}>
      <Typography variant="h1">
        {t('Compute connectivity from trajectories')}
      </Typography>

      <Typography variant="h3" component="h2" sx={{ mt: 4 }}>
        {t('Select a trajectory study')}
      </Typography>

      <Paper sx={{ mt: 4, mb: 6, width: '100%', bgcolor: 'white' }}>
        <FormControl fullWidth>
          <Select
            labelId="select-trajectory-label"
            id="select-trajectory"
            // @ts-ignore
            value={trajectoryId}
            onChange={handleSelectTrajectory}
          >
            {trajectories.map(trajectory => (
              <MenuItem key={trajectory.id} value={trajectory.id}>
                {trajectory.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Paper>

      <Typography variant="h3" component="h2">
        {t('Set connectivity parameters')}
      </Typography>

      {connectivity && (
        <>

          <Paper sx={{ mt: 4, p: 2, width: '100%' }}>
            <input
              accept=".json"
              id="connectivity-file"
              type="file"
              onChange={handleFileread}
              style={{ display: 'none' }}
            />

            <Button variant="contained" component="label" htmlFor="connectivity-file">
              {connectivityFile
                ? connectivityFile.name
                : <Trans>Choose a connectivity file...</Trans>}
            </Button>
          </Paper>

          <Paper sx={{ mt: 4, mb: 6, p: 2, width: '100%', bgcolor: 'white' }}>
            <Typography variant="h4" paragraph>
              {t('Select buffer type')}

              <HelpTooltip>
                <Trans>
                  Choose how to define the buffer zones around the locations of
                  network nodes
                </Trans>
                <Typography sx={{ my: 1 }} variant="caption" paragraph>
                  <Trans>
                    The buffer zones are used to assess the links between each pair
                    of nodes by evaluating if the trajectories starting from / arriving
                    to Node 1 go through the buffer zone around Node 2. Two options are available :
                    (i) the use of circular buffers (or spheric caps) of equal radii,
                    and (ii) the use of geographic buffers corresponding to geographic sectors of equal angles in latitude and longitude
                  </Trans>
                </Typography>
              </HelpTooltip>
            </Typography>

            <FormControl fullWidth sx={{ mb: 6 }}>
              <Select
                labelId="vertical-motion-label"
                id="vertical-motion"
                value={connectivity.method}
                onChange={handleChangeBuffer}
              >
                {BUFFER_TYPES.map(item => (
                  <MenuItem value={item.value} key={item.value}>
                    {item.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Typography variant="h4" paragraph>
              {t('Buffer radius')}

              <HelpTooltip>
                <Trans>
                  Specify the radius of the circular buffer (in km along the curvature of the Earth) or the half-angle
                  of the geographic buffer (in degrees of longitude and latitude)..
                </Trans>
              </HelpTooltip>
            </Typography>

            <Box sx={{ mx: 4 }}>
              <Slider
                aria-label="Custom marks"
                value={connectivity?.parameters.config.radius}
                min={RADIUS_BUFFER[connectivity.method].min}
                max={RADIUS_BUFFER[connectivity.method].max}
                step={RADIUS_BUFFER[connectivity.method].stepSize}
                valueLabelDisplay="on"
                onChange={handleChangeRadius}
                marks={getSliderMarks(RADIUS_BUFFER[connectivity.method])}
              />
            </Box>
          </Paper>

          <ListLocations
              locations={connectivity.locations}
              selectable={true}
              toggle={toggleLocation}
              selectAll={selectAllLocations}
              deselectAll={deselectAllLocations}
          />

          <ListDates
              dates={connectivity.dates}
              selectable={true}
              toggle={toggleDate}
              selectAll={selectAllDates}
              deselectAll={deselectAllDates}
          />
        </>
      )}

      {!connectivity && (
        <>
          <Paper sx={{ mt: 4, p: 2, width: '100%' }}>
            <Button variant="contained" component="label" htmlFor="trajectory-file" disabled>
                <Trans>Choose a connectivity file...</Trans>
            </Button>
          </Paper>

          <Paper sx={{ mt: 4, mb: 6, p: 2, width: '100%', bgcolor: 'white' }}>
            <Typography variant="h4" paragraph>
              {t('Select buffer type')}

              <HelpTooltip>
                <Trans>
                  Choose how to define the buffer zones around the locations of
                  network nodes
                </Trans>
                <Typography sx={{ my: 1 }} variant="caption" paragraph>
                  <Trans>
                    The buffer zones are used to assess the links between each pair
                    of nodes by evaluating if the trajectories starting from / arriving
                    to Node 1 go through the buffer zone around Node 2. Two options are available :
                    (i) the use of circular buffers (or spheric caps) of equal radii,
                    and (ii) the use of geographic buffers corresponding to geographic sectors of equal angles in latitude and longitude
                  </Trans>
                </Typography>
              </HelpTooltip>
            </Typography>

            <FormControl fullWidth sx={{ mb: 6 }}>
              <Select
                disabled
                labelId="vertical-motion-label"
                id="vertical-motion"
                onChange={handleChangeBuffer}
              >
                {BUFFER_TYPES.map(item => (
                  <MenuItem value={item.value} key={item.value}>
                    {item.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Typography variant="h4" paragraph>
              {t('Buffer radius')}

              <HelpTooltip>
                <Trans>
                  Specify the radius of the circular buffer (in km along the curvature of the Earth) or the half-angle
                  of the geographic buffer (in degrees of longitude and latitude).
                </Trans>
              </HelpTooltip>
            </Typography>

            <Box sx={{ mx: 4 }}>
              <Slider
                disabled
                aria-label="Custom marks"
                onChange={handleChangeRadius}
              />
            </Box>
          </Paper>

          <ListLocations locations={[]} />
        </>
      )}

      <Box sx={{ mb: 3, display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          size="large"
          disabled={
            !trajectoryId
            || connectivity?.locations.filter(location => location.selected).length === 0
            || connectivity?.dates.filter(date => date.selected).length === 0
          }
          onClick={submit}
          sx={{ mt: 2 }}
        >
          {t('Compute connectivity')}
        </Button>
      </Box>
    </Container>
  );
};

export default ComputeConnectivity;
