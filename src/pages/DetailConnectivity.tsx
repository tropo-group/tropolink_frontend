import React, { useState } from 'react';
import {
  Alert,
  Box,
  Button,
  Container,
  Divider,
  Grid,
  IconButton,
  Link,
  Paper, Snackbar,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { ContentCopy } from '@mui/icons-material';
import DownloadIcon from '@mui/icons-material/Download';

import { useKeycloak } from '@react-keycloak/web';
import { useTranslation } from 'react-i18next';
import { Link as RouterLink, useParams } from 'react-router-dom';

import ListLocations from '../components/ListLocations';
import Map from '../components/Map';
import exportJSON from '../utils/export-json';
import Loading from '../components/Loading';
import StatusChip from '../components/StatusChip';
import useConnectivity from '../hooks/useConnectivity';
import ListDates from "../components/ListDates";
import {formatDate} from "../utils/dates";
import ConnectivityProgress from "../components/ConnectivityProgress";
import {useSnackbar} from "notistack";
import {connectivityRawData} from "../utils/connectivity";

const DetailConnectivity = () => {
  const { t } = useTranslation();
  const { keycloak } = useKeycloak();
  const [busyDataQuery, setBusyDataQuery] = useState<String[]>([]);
  const [busyLogsDataQuery, setBusyLogsDataQuery] = useState<boolean>(false);
  const { enqueueSnackbar } = useSnackbar();

  const { id } = useParams();
  const { isLoading, connectivity } = useConnectivity(id);

  const points = connectivity?.locations.map(location => {
    const locationData = location.value.split(' ');
    return [locationData[2], locationData[3]];
  }) || [];

  if (isLoading) {
    return (
      <Loading />
    );
  }

  const copy = text => {
    navigator.clipboard.writeText(text);
    // TODO: snackbar
    // toast.displayToaster("ok", t("study.copy-success"))
  };

  const onClickExport = () => {
    if (connectivity) {
      // Create a blob of the data
      const json = connectivityRawData(connectivity);

      exportJSON(connectivity.name, json);
    }
  };

  if (!connectivity) {
    return null;
  }

  const [trajectoryFrom] = connectivity.parameters.trajectories_studies;

  const downloadData = async (format = 'json') => {
    setBusyDataQuery(prevList => Array.from(new Set([...prevList, format])));

    let endpoint;
    let extension;
    if (format === 'json') {
      endpoint = 'get_connectivity_json';
      extension = '.json';
    } else if (format === 'geojson') {
      endpoint = 'get_connectivity_geojson';
      extension = '.geojson';
    }  else if (format === 'table') {
      endpoint = 'get_connectivity_table';
      extension = '.csv';
    } else {
      return ;
    }
    const res = await fetch(
      `${import.meta.env.VITE_API_HOST}/connectivities/${connectivity.id}/${endpoint}/`,
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${keycloak.token}`,
        },
      },
    );

    if (res.status !== 200) {
      enqueueSnackbar(t("There was an error."), {
        variant: 'error',
        anchorOrigin: { horizontal: 'center', vertical: 'top' },
      });
      setBusyDataQuery(prevList => {
        const list = new Set(prevList);
        list.delete(format);
        return Array.from(list);
      });
      return ;
    }

    // Create a blob of the data
    const file = await res.blob();
    const a = document.createElement('a');
    a.href = URL.createObjectURL(file);
    a.download = `connectivity${connectivity.id}-${format}-${connectivity.slug}${extension}`;
    a.click();
    a.parentElement?.removeChild(a);

    setBusyDataQuery(prevList => {
      const list = new Set(prevList);
      list.delete(format);
      return Array.from(list);
    });
  };
  const downloadJSON = () => downloadData('json');
  const downloadGeoJSON = () => downloadData('geojson');
  const downloadTable = () => downloadData('table');

  const downloadLogs = async () => {
    setBusyLogsDataQuery(true);

    const res = await fetch(
      `${import.meta.env.VITE_API_HOST}/connectivities/${id}/get_logs/`,
      {
        headers: {
          Authorization: `Bearer ${keycloak.token}`,
        },
      },
    );

    if (res.status !== 200) {
      enqueueSnackbar(t("There was an error."), {
        variant: 'error',
        anchorOrigin: { horizontal: 'center', vertical: 'top' },
      });
      setBusyLogsDataQuery(false);
      return ;
    }
    const json = await res.json();
    window.open(`${import.meta.env.VITE_API_HOST}${json.url}`, '_blank');
    setBusyLogsDataQuery(false);
  };

  return (
    <Container sx={{ pb: 4 }}>
      <Grid container alignItems="center" justifyContent="space-between">
        <Grid item>
          <Typography variant="h1">
            {t('Connectivity')} {connectivity.name}
            {' '}
            <StatusChip status={connectivity.status} />
          </Typography>

          <Typography variant="subtitle2">
            {t('Trajectory')}:
            {' '}
            <Link component={RouterLink} to={`/trajectory/${trajectoryFrom.value}`}>
              {trajectoryFrom.label}
            </Link>
          </Typography>
        </Grid>
        <Grid item>
          <Button
            variant="outlined"
            onClick={() => onClickExport()}
            endIcon={<DownloadIcon />}
          >
            {t('Download config')}
          </Button>
          <Button
            variant="outlined"
            endIcon={<DownloadIcon />}
            disabled={connectivity.status !== 'succeeded'}
            color="secondary"
            sx={{ ml: 1 }}
            href="#download"
          >
            {t('Download results')}
          </Button>
        </Grid>
      </Grid>

      <Typography variant="h3" gutterBottom sx={{ mt: 4 }}>
        {t('Progress')}
        <ConnectivityProgress connectivity={connectivity} />
      </Typography>

      <Typography variant="h3" gutterBottom sx={{ mt: 4 }}>
        {t('Starting/arrival locations of forward/backward trajectories')}
      </Typography>

      <Map points={points} />

      <Box sx={{ mt: 1 }}>
        <ListLocations
          locations={connectivity.locations}
          afterTitle={(
            <IconButton
              onClick={() => copy(JSON.stringify(connectivity?.locations.map(location => location.value)))}
              aria-label="Copy locations"
              size="small"
            >
              <ContentCopy />
            </IconButton>
          )}
        />
      </Box>

      <Box sx={{ mt: 1 }}>
        <ListDates
          dates={connectivity.dates}
          afterTitle={(
            <IconButton
              onClick={() => copy(JSON.stringify(connectivity?.dates.map(date => formatDate(date.value))))}
              aria-label="Copy dates"
              size="small"
            >
              <ContentCopy />
            </IconButton>
          )}
        />
      </Box>

      <Typography variant="h3" gutterBottom sx={{ mt: 4 }}>
        {t('Parameters')}
      </Typography>

      <TableContainer component={Paper} sx={{ display: 'inline-block', width: 'auto', px: 2, pt: 1 }}>
        <Table size="small" sx={{ width: 'auto' }}>
          <TableBody>
            <TableRow>
              <TableCell
                component="th"
                variant="head"
              >
                {t('Buffer radius')}
              </TableCell>
              <TableCell>
                {connectivity.parameters.config.radius}
                {connectivity.parameters.config.method === 'buffer' && ' km'}
                {connectivity.parameters.config.method === 'geographic_buffer' && '°'}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell
                component="th"
                variant="head"
              >
                {t('Buffer type')}
              </TableCell>
              <TableCell>
                {connectivity.parameters.config.method === 'buffer'
                  ? 'Circular buffer'
                  : 'Geographic buffer'}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>

      {connectivity.status !== 'in_progress' && (
        <>
          <Divider sx={{ m: 4 }} />

          <Typography variant="h3" gutterBottom sx={{ mt: 4 }} id="download">
            {t('Download results')}
          </Typography>

          <Paper sx={{ px: 2, py: 1 }}>
            <Box sx={{ mt: 1, mb: 2 }}>
              Warning: if your study is made up of many points/dates, it will take some time to generate the
              archive for logs before you can download it.<br />
              If you get a "timeout" during the operation, you may
              have to wait about an hour before the archive is ready to download.
            </Box>
            <Box sx={{ display: 'inline' }}>
              {connectivity.status === 'succeeded' && (
                <>
                  <LoadingButton
                    loading={busyDataQuery.includes('json')}
                    variant="outlined"
                    onClick={downloadJSON}
                    sx={{ textTransform: 'none' }}
                  >
                    as JSON
                  </LoadingButton>
                  {' '}
                  <LoadingButton
                    loading={busyDataQuery.includes('geojson')}
                    variant="outlined"
                    onClick={downloadGeoJSON}
                    sx={{ textTransform: 'none' }}
                  >
                    as GeoJSON
                  </LoadingButton>
                  {' '}
                  <LoadingButton
                    loading={busyDataQuery.includes('table')}
                    variant="outlined"
                    onClick={downloadTable}
                    sx={{ textTransform: 'none' }}
                  >
                    as Table
                  </LoadingButton>
                  {' '}
                </>
              )}
              <LoadingButton
                loading={busyLogsDataQuery}
                variant="outlined"
                onClick={() => downloadLogs()}
                sx={{ textTransform: 'none' }}
              >
                {t('Download all logs')}
              </LoadingButton>
            </Box>
          </Paper>
        </>
      )}
    </Container>
  );
};

export default DetailConnectivity;
