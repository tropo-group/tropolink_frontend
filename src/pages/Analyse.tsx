import React from 'react';
import { useParams } from 'react-router-dom';
import { Box } from '@mui/material';
import { useKeycloak } from '@react-keycloak/web';

export const Analyse = () => {
  const { keycloak } = useKeycloak();
  const { language } = useParams();

  const jupyterEndpoint = import.meta.env.VITE_JUPYTER_HOST;

  const jupyterNotebook = language === 'r'
    ? import.meta.env.VITE_JUPYTER_NOTEBOOK_R
    : import.meta.env.VITE_JUPYTER_NOTEBOOK_PYTHON;

  // Only refresh token because a cookie is too long otherwise.
  const iframeSrc = `${jupyterEndpoint}/user-redirect/notebooks/notebook/${jupyterNotebook}?refresh_token="${keycloak.refreshToken}"`;

  return (
    <Box
      component="iframe"
      sx={{
        border: 0,
        width: '100%',
        flex: 1,
      }}
      src={iframeSrc}
      title="jupyter"
      allowFullScreen
    />
  );
};

export default Analyse;
