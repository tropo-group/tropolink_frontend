import React from 'react';
import { Delete } from '@mui/icons-material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import EditIcon from '@mui/icons-material/Edit';
import {
  AvatarGroup,
  Box,
  Button,
  Collapse,
  Container,
  IconButton,
  LinearProgress,
  Link,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  useTheme,
} from '@mui/material';

import { useTranslation } from 'react-i18next';
import { Link as RouterLink } from 'react-router-dom';

import { useConfirm } from 'material-ui-confirm';

import StatusChip from '../components/StatusChip';
import { ComputationId } from '../entities/computation';
import { Study } from '../entities/study';
import useStudies from '../hooks/useStudies';
import StudyEditor from '../components/StudyEditor';
import TrajectoryProgress from "../components/TrajectoryProgress";
import ConnectivityProgress from "../components/ConnectivityProgress";

interface RowProps {
  study: Study;
  onDeleteStudy: (id: ComputationId) => void;
  onPatchStudy: (args: Object) => void;
  onDeleteConnectivity: (id: ComputationId) => void;
}

export const Row = ({
  study,
  onDeleteStudy,
  onDeleteConnectivity,
  onPatchStudy,
}: RowProps) => {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const confirm = useConfirm();
  const { t } = useTranslation();

  const defaultConfirmDeleteOptions = React.useMemo(
    () => ({
      description: t('This action is irreversible!'),
      confirmationText: t('Delete'),
      cancellationText: t('Cancel'),
      confirmationButtonProps: { variant: 'contained', color: 'error' },
      cancellationButtonProps: { variant: 'contained' },
    }),
    [t],
  );

  const toggleRow = React.useCallback(
    event => {
      event.stopPropagation();
      setOpen(prev => !prev);
    },
    [],
  );

  const handleConnectivityDelete = React.useCallback(
    id => event => {
      event.stopPropagation();

      // @ts-ignore
      confirm({
        ...defaultConfirmDeleteOptions,
        title: t('Are you sure you want to delete this connectivity?'),
      }).then(() => onDeleteConnectivity(id)).catch(() => {});
    },
    [confirm, defaultConfirmDeleteOptions, t, onDeleteConnectivity],
  );

  const handleStudyDelete = React.useCallback(
    event => {
      event.stopPropagation();

      // @ts-ignore
      confirm({
        ...defaultConfirmDeleteOptions,
        title: t('Are you sure you want to delete this study?'),
      }).then(() => onDeleteStudy(study.id)).catch(() => {});
    },
    [confirm, defaultConfirmDeleteOptions, onDeleteStudy, study.id, t],
  );

  const handlePatchStudy = event => {
    event.stopPropagation();
    onPatchStudy({
      id: study.id,
      description: `${study.description}·`,
    });
  };

  if (!study?.trajectory) {
    return null;
  }

  return (
    <>
      <TableRow
        hover
        sx={{ '& > *': { borderBottom: 'unset !important' } }}
        onClick={toggleRow}
      >
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={toggleRow}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {study.name}
        </TableCell>
        <TableCell>{study.creation_date}</TableCell>
        <TableCell>{study.modification_date}</TableCell>
        <TableCell>
          <Button
            size="small"
            onClick={handlePatchStudy}
            sx={{ mr: 1, minWidth: 0, opacity: 0.2, '&:hover': { opacity: 1 } }}
          >
            <EditIcon fontSize="small" />
          </Button>
          {study.description}
        </TableCell>
        <TableCell>
          <AvatarGroup spacing="small" sx={{ justifyContent: 'flex-end' }}>
            <StatusChip
              mini
              status={study.trajectory.status}
              alt={study.trajectory.name}
            />

            {study.connectivities.map(connectivity => (
              <StatusChip
                key={connectivity.id}
                mini
                status={connectivity.status}
                dim
                alt={connectivity.name}
              />
            ))}
          </AvatarGroup>
        </TableCell>
        <TableCell align="right">
          <IconButton
            onClick={handleStudyDelete}
            aria-label="Delete study"
          >
            <Delete />
          </IconButton>
        </TableCell>
      </TableRow>

      <TableRow
        sx={{ backgroundColor: theme.palette.grey[200] }}
      >
        <TableCell sx={{ padding: 0, pl: 15 }} colSpan={7}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>{t('Status')}</TableCell>
                    <TableCell>{t('Type')}</TableCell>
                    <TableCell>{t('Name')}</TableCell>
                    <TableCell>{t('Date')}</TableCell>
                    <TableCell>{t('Algorithm')}</TableCell>
                    <TableCell />
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <TrajectoryProgress trajectory={study.trajectory} />
                    </TableCell>
                    <TableCell>{t('Trajectory')}</TableCell>
                    <TableCell>
                      <Link
                        component={RouterLink}
                        to={`/trajectory/${study.trajectory.id}`}
                      >
                        {study.trajectory.name}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {study.trajectory.creation_date}
                    </TableCell>
                    <TableCell>{t('hysplit')}</TableCell>
                    <TableCell />
                  </TableRow>
                  {study.connectivities.map(connectivity => (
                    <TableRow key={connectivity.id}>
                      <TableCell>
                        <ConnectivityProgress connectivity={connectivity} />
                      </TableCell>
                      <TableCell>{t('Connectivity')}</TableCell>
                      <TableCell>
                        <Link
                          component={RouterLink}
                          to={`/connectivity/${connectivity.id}`}
                        >
                          {connectivity.name}
                        </Link>
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {connectivity.creation_date}
                      </TableCell>
                      <TableCell>{connectivity.method}</TableCell>
                      <TableCell align="right">
                        <IconButton
                          onClick={handleConnectivityDelete(connectivity.id)}
                          aria-label="Delete connectivity"
                          size="small"
                          sx={{ my: -2 }}
                        >
                          <Delete />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

type Order = 'asc' | 'desc';

const StudyList = () => {
  const { t } = useTranslation();

  const {
    isLoading,
    studies,
    deleteStudy,
    deleteConnectivity,
    patchStudy,
  } = useStudies();

  const [order, setOrder] = React.useState<Order>('desc');
  const [orderBy, setOrderBy] = React.useState<string>('modification_date');
  const [studyToEdit, setStudyToEdit] = React.useState<null | Study>();

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const createSortHandler = property => (event: React.MouseEvent<unknown>) => {
    handleRequestSort(event, property);
  };

  const sortedStudies = React.useMemo(
    () => (studies || []).slice().sort(
      // @ts-expect-error
      ({ [orderBy]: a = '' }, { [orderBy]: b = '' }) => (
        order === 'asc' ? a.localeCompare(b) : b.localeCompare(a)),
    ),
    [order, orderBy, studies],
  );

  return (
    <Container maxWidth="xl" sx={{ pt: 4 }}>
      <TableContainer component={Paper} sx={{ position: 'relative' }}>
        {isLoading && (
          <LinearProgress sx={{ m: 'auto', position: 'absolute', bottom: 0, left: 0, right: 0 }} />
        )}
        <Table aria-label="collapsible table" size="small">
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>
                <TableSortLabel
                  active={orderBy === 'name'}
                  direction={orderBy === 'name' ? order : 'asc'}
                  onClick={createSortHandler('name')}
                >
                  {t('Name')}
                </TableSortLabel>
              </TableCell>
              <TableCell>
                <TableSortLabel
                  active={orderBy === 'creation_date'}
                  direction={orderBy === 'creation_date' ? order : 'asc'}
                  onClick={createSortHandler('creation_date')}
                >
                  {t('Creation date')}
                </TableSortLabel>
              </TableCell>
              <TableCell>
                <TableSortLabel
                  active={orderBy === 'modification_date'}
                  direction={orderBy === 'modification_date' ? order : 'asc'}
                  onClick={createSortHandler('modification_date')}
                >
                  {t('Modification date')}
                </TableSortLabel>
              </TableCell>
              <TableCell>{t('Description')}</TableCell>
              <TableCell>{t('Status')}</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {studies && sortedStudies.map(study => (
              <Row
                key={study.id}
                study={study}
                onDeleteStudy={id => deleteStudy(id)}
                onPatchStudy={() => setStudyToEdit(studies.find(({ id }) => (id === study.id)))}
                onDeleteConnectivity={id => deleteConnectivity(id)}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <StudyEditor
        study={studyToEdit}
        // @ts-ignore
        onValidation={patchStudy}
        onClose={() => setStudyToEdit(null)}
      />
    </Container>
  );
};

export default StudyList;
