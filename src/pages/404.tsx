import React from 'react';
import Typography from '@mui/material/Typography';

const Page404 = () => <Typography variant="h1">Page not found</Typography>;

export default Page404;
