import { DateRangePicker, DesktopDatePicker } from '@mui/lab';
import {
  Box,
  Button,
  Chip,
  Tab,
  Tabs,
  TextField,
  Typography,
} from '@mui/material';
import { DeleteForever } from '@mui/icons-material';
import { Trans, useTranslation } from 'react-i18next';
import { useSnackbar } from 'notistack';
import React, {ChangeEvent, useEffect, useState} from 'react';
import HelpTooltip from '../../../components/HelpTooltip';
import { a11yProps, TabPanel } from '../../../components/TabPanel';
import {
  DatePickerDate,
  formatDate,
  getDates,
  sortDates,
  uniqueDates,
} from '../../../utils/dates';
import { handleFile } from '../../../utils/file';

interface DatesSelectionProps {
  dates: Date[];
  setDates: (prevState: Date[]) => void;
}

const DatesSelection: React.FC<DatesSelectionProps> = ({ dates, setDates }) => {
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const [tab, setTab] = useState<number>(0);
  // const [fileImport, setFileImport] = useState<File | null>(null);
  const [startRangeDate, setStartRangeDate] = useState<DatePickerDate>(null);
  const [endRangeDate, setEndRangeDate] = useState<DatePickerDate>(null);
  const [singleDate, setSingleDate] = useState<DatePickerDate>(null);

  function removeDuplicateDate (processingDates: Date[]) {
    const allDates = [...dates, ...processingDates];
    const sortedDates = sortDates(allDates);
    return uniqueDates(sortedDates);
  }

  useEffect(() => {
    if (startRangeDate !== null && endRangeDate !== null) {
      const datesUnique = removeDuplicateDate(getDates([startRangeDate, endRangeDate]));
      setDates([...datesUnique]);
    }
  }, [startRangeDate, endRangeDate]);

  function onStartRangeDateChange (newDate: DatePickerDate) {
    const validateDates = newDate !== null;

    if (validateDates) {
      setStartRangeDate(newDate);
    }
  }

  function onEndRangeDateChange (newDate: DatePickerDate) {
    const validateDates = newDate !== null;

    if (validateDates) {
      setEndRangeDate(newDate);
    }
  }

  function onDateChange (newDate) {
    const validateDates = newDate !== null;

    if (validateDates) {
      const datesUnique = removeDuplicateDate([newDate]);
      setSingleDate(newDate);
      setDates([...datesUnique]);
    }
  }

  function parseDates (str: string) {
    const array = str.trim().split('\n')
      .map(date => date.trim());

    const datesArray = array.map(date => new Date(date));
    const datesUnique = removeDuplicateDate(datesArray);
    setDates([...datesUnique]);
  }

  function displayError () {
    enqueueSnackbar(t("This file doesn't contain valid dates."), {
      variant: 'error',
      anchorOrigin: { horizontal: 'center', vertical: 'top' },
    });
  }

  function handleDatesFile (e: ChangeEvent<HTMLInputElement>) {
    if (e.target.files?.length) {
      const file = e.target.files[0];
      // setFileImport(file);

      handleFile(file)
        .then(str => {
          try {
            parseDates(str);
          } catch (err) {
            displayError();
          }
        })
        .catch(() => {
          displayError();
        });
    }
  }

  function removeDate (item: Date) {
    const newDates = dates;
    const indexItemToRemove = newDates.findIndex(
      date => date.getTime() === item.getTime(),
    );
    newDates.splice(indexItemToRemove, 1);
    setDates([...newDates]);
  }

  function removeAllDates () {
    setDates([]);
  }

  return (
    <>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={tab}
          onChange={(event, newValue) => setTab(newValue)}
        >
          <Tab label="Selection by file" {...a11yProps(0)} />
          <Tab label="Selection by period" {...a11yProps(1)} />
          <Tab label="Select single days" {...a11yProps(1)} />
        </Tabs>
      </Box>
      <TabPanel value={tab} index={0}>
        <input
          // accept=".json"
          id="dates-file"
          type="file"
          onChange={handleDatesFile}
          style={{ display: 'none' }}
        />

        <Button variant="contained" component="label" htmlFor="dates-file">
          {t('Import dates from file…')}
        </Button>

        <HelpTooltip>
          <Typography sx={{ my: 1 }} variant="caption" paragraph>
            <Trans>
              Dates must conform to the international standard (ISO 8601)
            </Trans>
          </Typography>
          <Typography sx={{ my: 1 }} variant="subtitle2" paragraph>
            <Trans>Standard: YYYY-MM-DD</Trans>
          </Typography>
          <Typography sx={{ my: 1 }} variant="caption" paragraph>
            <Trans>Example: 2019-11-22</Trans>
          </Typography>
        </HelpTooltip>
      </TabPanel>
      <TabPanel value={tab} index={1}>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <DesktopDatePicker
            minDate={new Date('2007-09-01')}
            maxDate={endRangeDate || new Date()}
            label="Start date"
            inputFormat="yyyy-MM-dd"
            value={startRangeDate}
            onChange={onStartRangeDateChange}
            renderInput={params => <TextField {...params} />}
          />
          <Box component="span" sx={{ mx: 2 }}> to </Box>
          <DesktopDatePicker
            minDate={startRangeDate || new Date('2007-09-01')}
            maxDate={new Date()}
            label="End date"
            inputFormat="yyyy-MM-dd"
            value={endRangeDate}
            onChange={onEndRangeDateChange}
            renderInput={params => <TextField {...params} />}
          />
        </Box>
      </TabPanel>
      <TabPanel value={tab} index={2}>
        <DesktopDatePicker
          maxDate={new Date()}
          label="Date"
          inputFormat="yyyy-MM-dd"
          value={singleDate}
          onChange={onDateChange}
          renderInput={params => <TextField {...params} />}
        />
      </TabPanel>
      <Box sx={{ pb: 1, px: 2 }}>
        {dates.map(date => (
          <Chip
            key={date.getTime()}
            sx={{ mr: 1, mb: 1 }}
            label={formatDate(date)}
            onDelete={() => removeDate(date)}
            size="small"
          />
        ))}

        {Boolean(dates.length) && (
          <Box sx={{ textAlign: 'right' }}>
            <Button
              sx={{ mr: 1, mb: 1 }}
              color="error"
              size="small"
              onClick={removeAllDates}
              variant="contained"
              startIcon={<DeleteForever />}
            >
              <Trans>Remove all</Trans>
            </Button>
          </Box>
        )}

        {!dates.length && (
          <Box>
            <Typography sx={{ mr: 1, mb: 1, color: 'text.secondary' }}>
              <Trans>No date provided</Trans>
            </Typography>
          </Box>
        )}
      </Box>
    </>
  );
};

export default React.memo(DatesSelection);
