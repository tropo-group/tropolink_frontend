import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Slider,
  TextField,
  Typography,
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import React from 'react';
import { Hysplit, TrajectoryError } from '../../../entities/trajectory';
import HelpTooltip from '../../../components/HelpTooltip';
import { VERTICAL_MOTION, RUNTIME } from '../../../computation.config';
import { getSliderMarks } from '../../../utils/form';

interface HysplitConfigProps extends Hysplit {
  setHysplit: (hysplit: Partial<Hysplit>) => void;
  errors: Partial<TrajectoryError>;
}

const HysplitConfig: React.FC<HysplitConfigProps> = ({
  setHysplit,
  errors,
  ...hysplit
}) => {
  const { t } = useTranslation();

  function handleChangeHour (e) {
    setHysplit({
      hour_utc: e.target.value,
    });
  }

  function handleChangeTopOfModel (e) {
    setHysplit({
      top_of_model: e.target.value,
    });
  }

  function handleChangeVerticalMotion (e) {
    setHysplit({
      vertical_motion: e.target.value,
    });
  }

  function handleChangeRuntime (e) {
    setHysplit({
      runtime: e.target.value,
    });
  }

  return (
    <Box sx={{ p: 3, '& .MuiTextField-root': { m: 1, width: '25ch' } }}>
      <Box sx={{ m: 3 }}>
        <Typography variant="h5">
          {t('Trajectory duration')}
          <HelpTooltip>
            <Typography sx={{ my: 1 }} variant="caption" paragraph>
              {t('Specify the duration of trajectories in hours (negative values for backward trajectories).')}
            </Typography>
          </HelpTooltip>
        </Typography>

        <Slider
          aria-label="Runtime"
          value={hysplit.runtime}
          min={RUNTIME.min}
          max={RUNTIME.max}
          step={RUNTIME.stepSize}
          valueLabelDisplay="on"
          onChange={handleChangeRuntime}
          marks={getSliderMarks(RUNTIME)}
        />
      </Box>

      <Box sx={{ display: 'flex' }}>
        <Box sx={{ flex: '1', display: 'flex', mx: 1, my: 2 }}>
          <TextField
            id="hour-utc"
            sx={{ flex: 1 }}
            label={t('Starting/Arrival hour (UTC)')}
            type="number"
            value={hysplit.hour_utc}
            onChange={handleChangeHour}
            error={!!errors.hour_utc}
            helperText={
              !!errors.hour_utc
              && 'This field must be a number between 0 and 23.'
            }
          />
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <HelpTooltip>
              Specify time (in hours between 0:00 UTC and 23:00 UTC) of the starting/arrival
              of forward/backward trajectories.
            </HelpTooltip>
          </Box>
        </Box>

        <Box sx={{ flex: '1', display: 'flex', mx: 1, my: 2 }}>
          <TextField
            id="top-of-model"
            label={t('Top of model domain (m)')}
            sx={{ flex: 1 }}
            type="number"
            value={hysplit.top_of_model}
            onChange={handleChangeTopOfModel}
          />
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <HelpTooltip>
              Set the top level (height in meters) of the 3D domain above the ground.
              meteorological data are processed thus speeding up the
              computation. Trajectories will terminate when they reach this
              level. A secondary use of this parameter is to set the model's
              internal scaling height - the height at which the internal sigma
              surfaces go flat relative to terrain.
            </HelpTooltip>
          </Box>
        </Box>
      </Box>

      <Box sx={{ display: 'flex', mx: 1, my: 2 }}>
        <FormControl fullWidth>
          <InputLabel id="vertical-motion-label">
            {t('Vertical motion method')}
          </InputLabel>
          <Select
            labelId="vertical-motion-label"
            id="vertical-motion"
            value={hysplit.vertical_motion}
            label={t('Method')}
            onChange={handleChangeVerticalMotion}
          >
            {VERTICAL_MOTION.map(item => (
              <MenuItem value={item.value} key={item.value}>
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <HelpTooltip>
            Set the type of vertical motion method the model should use in its
            calculation. For most simulations, choose the default "Model
            vertical velocity" that will use the vertical velocity field from
            meterological data. See Hysplit Help page for additional information.
          </HelpTooltip>
        </Box>
      </Box>
    </Box>
  );
};

export default React.memo(HysplitConfig);
