import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Tab,
  Tabs,
} from '@mui/material';
import { Trans, useTranslation } from 'react-i18next';
import { useSnackbar } from 'notistack';
import React, { useState } from 'react';
import { a11yProps, TabPanel } from '../../../components/TabPanel';
import { ComputationId } from '../../../entities/computation';
import {
  Trajectory,
  TrajectoryModel,
  TrajectoryRaw,
} from '../../../entities/trajectory';
import { handleJSONFile } from '../../../utils/file';
import {
  getDataFromTrajectory,
  parseTrajectory,
} from '../../../utils/trajectory';

interface LoadTrajectoryProps {
  trajectories: Trajectory[];
  setTrajectory: React.Dispatch<React.SetStateAction<TrajectoryModel>>;
}

const LoadTrajectory: React.FC<LoadTrajectoryProps> = ({
  setTrajectory,
  trajectories,
}) => {
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();

  const [tabPrevious, setTabPrevious] = useState(0);

  const [hysplitFile, setHysplitFile] = useState<File | null>(null);
  const [previousTrajectory, setPreviousTrajectory] = useState<
  ComputationId | ''
  >('');

  function setTrajectorySpec (trajectory: TrajectoryModel) {
    setTrajectory(prevState => ({
      ...trajectory,
      name: prevState.name,
    }));
    enqueueSnackbar(t('The specifications have been imported.'), {
      variant: 'success',
      anchorOrigin: { horizontal: 'center', vertical: 'top' },
    });
  }

  function handleSelect (e: SelectChangeEvent<number>) {
    const id = e.target.value;
    if (typeof id === 'number') {
      setPreviousTrajectory(id);
      setHysplitFile(null);
    }

    const trajectory = trajectories.find(traj => traj.id === id);

    if (trajectory) {
      setTrajectorySpec(getDataFromTrajectory(trajectory));
    }
  }

  const handleFileread = e => {
    if (e.target.files?.length) {
      const file = e.target.files[0];
      setHysplitFile(file);
      handleJSONFile(file).then(data => {
        try {
          const trajectory = parseTrajectory(data as unknown as TrajectoryRaw);
          setPreviousTrajectory('');
          setTrajectorySpec(trajectory);
        } catch (err) {
          console.error(err);
          console.warn(t('form.error', { context: 'json' }));
          enqueueSnackbar(t("This file doesn't contain a valid json."), {
            variant: 'error',
            anchorOrigin: { horizontal: 'center', vertical: 'top' },
          });
        }
      });
    }
  };

  return (
    <>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={tabPrevious}
          onChange={(event, newValue) => setTabPrevious(newValue)}
        >
          <Tab label="From existing study" {...a11yProps(0)} />
          <Tab label="From file" {...a11yProps(1)} />
        </Tabs>
      </Box>
      <TabPanel value={tabPrevious} index={0}>
        <FormControl fullWidth>
          <InputLabel id="select-trajectory-label">
            {t('Select a study name...')}
          </InputLabel>
          <Select
            labelId="select-trajectory-label"
            id="select-trajectory"
            // @ts-ignore
            value={previousTrajectory}
            label={t('Select a study name...')}
            onChange={handleSelect}
          >
            {trajectories.map(trajectory => (
              <MenuItem key={trajectory.id} value={trajectory.id}>
                {trajectory.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </TabPanel>
      <TabPanel value={tabPrevious} index={1}>
        <input
          accept=".json"
          id="trajectory-file"
          type="file"
          onChange={handleFileread}
          style={{ display: 'none' }}
        />

        <Button variant="contained" component="label" htmlFor="trajectory-file">
          {hysplitFile
            ? hysplitFile.name
            : <Trans>Import trajectory specs from file...</Trans>}
        </Button>
      </TabPanel>
    </>
  );
};

export default React.memo(LoadTrajectory);
