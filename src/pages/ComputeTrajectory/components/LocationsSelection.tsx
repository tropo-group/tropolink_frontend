import {
  Box,
  Button,
  Chip,
  FormHelperText,
  InputAdornment,
  Tab,
  Tabs,
  TextField,
  Typography,
} from '@mui/material';
import {
  Delete,
  DeleteForever,
} from '@mui/icons-material';
import { Trans, useTranslation } from 'react-i18next';
import { useSnackbar } from 'notistack';
import React, { ChangeEvent, useState } from 'react';
import HelpTooltip from '../../../components/HelpTooltip';
import { a11yProps, TabPanel } from '../../../components/TabPanel';
import { ComputationLocation } from '../../../entities/computation';
import { handleFile } from '../../../utils/file';
import { parseLocations } from '../../../utils/locations';

interface LocationsSelectionProps {
  locations: ComputationLocation[];
  setLocations: (newLocations: ComputationLocation[]) => void;
}

// const floor = (num, suffix = '') => str => {
//   const r = Number.parseFloat(str);
//   const pow = 10 ** num;

//   return (Math.floor(r * pow) / pow) + suffix;
// };

const prettifyLocation = locationString => {
  try {
    const [code, name, ...rest] = locationString.trim().split(/\s+/);
    return (
      <>
        <Box component="span">{code}</Box>{' '}
        <Box component="span">{name}</Box>{' '}
        <Box component="span" sx={{ opacity: 0.5 }}>{rest.join(' ')}</Box>
      </>
    );
  } catch (err) {
    console.error(err);
  }
  return locationString;
};

const LocationsSelection: React.FC<LocationsSelectionProps> = ({
  locations,
  setLocations,
}) => {
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const addLocationRef = React.useRef<HTMLElement>();
  const [locationString, setLocationString] = React.useState<string>('');

  const handleLocationStringChange = event => {
    setLocationString(event.target.value);
  };

  const [tab, setTab] = useState<number>(0);
  // const [fileImport, setFileImport] = useState<File | null>(null);

  function displayError () {
    enqueueSnackbar(t("This file doesn't contain valid locations."), {
      variant: 'error',
      anchorOrigin: { horizontal: 'center', vertical: 'top' },
    });
  }

  function handleLocationsFile (e: ChangeEvent<HTMLInputElement>) {
    if (e.target.files?.length) {
      const file = e.target.files[0];
      // setFileImport(file);

      handleFile(file)
        .then(str => {
          try {
            const array = str.trim().split('\n');
            const newLocations = parseLocations(array);
            setLocations([...locations, ...newLocations]);
          } catch {
            displayError();
          }
        })
        .catch(() => {
          displayError();
        });
    }
  }

  function removeLocation (id: string) {
    setLocations(locations.filter(loc => loc.id !== id));
  }

  const removeInvalidLocations = () =>
    setLocations(locations.filter(loc => loc.valid));

  function removeAllLocations () {
    setLocations([]);
  }

  const addLocation = str => {
    setLocations([...locations, ...parseLocations([str])]);
    setLocationString('');
  };

  return (
    <>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={tab}
          onChange={(event, newValue) => setTab(newValue)}
        >
          <Tab label="Selection by file" {...a11yProps(0)} />
          <Tab label="Selection by list" {...a11yProps(1)} />
        </Tabs>
      </Box>
      <TabPanel value={tab} index={0}>
        <input
          id="locations-file"
          type="file"
          onChange={handleLocationsFile}
          style={{ display: 'none' }}
        />

        <Button variant="contained" component="label" htmlFor="locations-file">
          {t('Import coordinates from file…')}
        </Button>

        <HelpTooltip>
          <Typography sx={{ my: 1 }} variant="caption" paragraph>
            <Trans>Fields should be separated by at least one space.</Trans>
          </Typography>

          <Typography sx={{ my: 1 }} variant="caption" paragraph>
            <Trans>Station names must not contain any space character.</Trans>
          </Typography>

          <Typography sx={{ my: 1 }} variant="caption" paragraph>
            <Trans>
              Coordinates should be in degrees in dot-decimal format (WGS84;
              EPSG4326). Latitudes b/n -90 and 90 degrees (negative values for
              Southern hemisphere). Longitudes b/n -180 and 180 degrees
              (negative values for Western hemisphere wrt Greench meridian).
            </Trans>
          </Typography>
          <Typography sx={{ my: 1 }} variant="subtitle2" paragraph>
            <Trans>
              Norm: Code Name Latitude[deg] Longitude[deg]
              Arrival/starting_height_above_ground_level[m]
            </Trans>
          </Typography>
          <Typography sx={{ my: 1 }} variant="caption" paragraph>
            <Trans>Example: STA StationName 50.28 2.78 1000</Trans>
          </Typography>
        </HelpTooltip>
      </TabPanel>

      <TabPanel value={tab} index={1}>
        <TextField
          inputRef={addLocationRef}
          label="Location string"
          fullWidth
          helperText="Code Name Latitude[deg] Longitude[deg] Arrival/starting_height_above_ground_level[m]"
          onChange={handleLocationStringChange}
          value={locationString}
          onKeyPress={event => {
            if (event.key === 'Enter' && parseLocations([locationString])?.[0]?.valid) {
              addLocation(locationString);
            }
          }}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Button
                  variant="contained"
                  disabled={!parseLocations([locationString])?.[0]?.valid}
                  onClick={() => addLocation(locationString)}
                >
                  {t('Add location')}
                </Button>
              </InputAdornment>
            ),
          }}
        />
      </TabPanel>

      <Box sx={{ pb: 1, px: 2 }}>
        {locations.map(({ value, id, valid }) => (
          <Chip
            key={id}
            sx={{ mr: 1, mb: 1 }}
            label={prettifyLocation(value) || (
              <Box component="span" sx={{ fontStyle: 'italic' }}>{t('invalid')}</Box>
            )}
            onDelete={() => removeLocation(id)}
            variant={valid ? 'filled' : 'outlined'}
            color={valid ? 'default' : 'error'}
            size="small"
          />
        ))}

        {Boolean(locations.length) && (
          <Box sx={{ textAlign: 'right' }}>
            {(locations.some(loc => !loc.valid)) && (
              <Button
                sx={{ mr: 1, mb: 1 }}
                color="error"
                size="small"
                onClick={removeInvalidLocations}
                variant="outlined"
                startIcon={<Delete />}
              >
                <Trans>Remove invalid locations</Trans>
              </Button>
            )}
            <Button
              sx={{ mr: 1, mb: 1 }}
              color="error"
              size="small"
              onClick={removeAllLocations}
              variant="contained"
              startIcon={<DeleteForever />}
            >
              <Trans>Remove all locations</Trans>
            </Button>
          </Box>
        )}

        {!locations.length && (
          <Box>
            <Typography sx={{ mr: 1, mb: 1, color: 'text.secondary' }}>
              <Trans>No location provided</Trans>
            </Typography>
          </Box>
        )}

        {locations.some(({ valid }) => valid === false) && (
          <FormHelperText error sx={{ mr: 1, mb: 1, color: 'text.secondary' }}>
            <Trans>Please remove the incomplete locations</Trans>
          </FormHelperText>
        )}
      </Box>
    </>
  );
};

export default React.memo(LocationsSelection);
