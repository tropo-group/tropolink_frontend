import {
  Box,
  Button,
  Container,
  Paper,
  TextField,
  Typography,
} from '@mui/material';
import { useKeycloak } from '@react-keycloak/web';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

import { ComputationLocation } from '../../entities/computation';
import {
  Hysplit,
  Trajectory,
  TrajectoryError,
  TrajectoryFormData,
  TrajectoryModel,
} from '../../entities/trajectory';
import { fetchTrajectories, saveTrajectory } from '../../utils/api';
import { getErrorMessage } from '../../utils/form';
import {
  parseTrajectory,
  trajectoryFormData,
  validateComputeTrajectory,
} from '../../utils/trajectory';
import DatesSelection from './components/DatesSelection';
import HysplitConfig from './components/HysplitConfig';
import LoadTrajectory from './components/LoadTrajectory';
import LocationsSelection from './components/LocationsSelection';
import HelpTooltip from "../../components/HelpTooltip";

const defaultTrajectory = {
  runtime: -24,
  hour_utc: 12,
  vertical_motion: 0,
  top_of_model: 10000,
  dates: [],
  locations: [],
  name: '',
  slug: '',
  description: '',
};

const ComputeTrajectory = () => {
  const { t } = useTranslation();
  const { keycloak } = useKeycloak();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const [studyDescription, setStudyDescription] = useState<string>('');

  const [trajectories, setTrajectories] = useState<Trajectory[]>([]);
  const [trajectory, setTrajectory] = useState<TrajectoryModel>(defaultTrajectory);
  const [errors, setErrors] = useState<Partial<TrajectoryError>>({});
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [loading, setLoading] = useState(false);

  function setDates (newDates: Date[]): void {
    setTrajectory({
      ...trajectory,
      dates: newDates,
    });
  }

  function setLocations (newLocations: ComputationLocation[]): void {
    setTrajectory({
      ...trajectory,
      locations: newLocations,
    });
  }

  function setHysplit (newHysplit: Partial<Hysplit>): void {
    setTrajectory({
      ...trajectory,
      ...newHysplit,
    });
  }

  function handleChangeName (e): void {
    setTrajectory({
      ...trajectory,
      name: e.target.value,
    });
  }

  const handleDescriptionChange = event => {
    setStudyDescription(event.target.value);
  };

  useEffect(() => {
    let mounted = true;

    function parseTrajectories (response: any[]): Trajectory[] {
      return response.map(parseTrajectory);
    }

    const getTrajectories = async (isMounted, signal) => {
      if (isMounted) {
        const response = await fetchTrajectories(keycloak, signal);

        setLoading(true);
        // TODO: manage error
        if (response && response.length > 0) {
          setTrajectories(parseTrajectories(response));
        }
      }
    };

    /** Allow to abort fetch if component is being unmounted */
    const abortController = new AbortController();
    getTrajectories(mounted, abortController.signal);

    return () => {
      mounted = false;
      abortController.abort();
    };
  }, [keycloak]);

  useEffect(() => {
    const err = validateComputeTrajectory(trajectories, trajectory);
    setErrors(err);
  }, [trajectories, trajectory]);

  function isValid () {
    return Object.keys(errors).length > 0;
  }

  async function submit () {
    setLoading(true);

    const formData: TrajectoryFormData = trajectoryFormData({
      ...trajectory,
      description: studyDescription,
    });

    await saveTrajectory(keycloak, formData);
    setLoading(false);

    enqueueSnackbar(t('The study has been created.'), {
      variant: 'success',
      anchorOrigin: { horizontal: 'center', vertical: 'top' },
    });

    navigate('/list');
  }

  return (
    <Container sx={{ pt: 4 }}>
      <Typography variant="h1">
        {t('Trajectory')}
      </Typography>

      <Box sx={{ mt: 4, mb: 6 }}>
        <TextField
          id="study-name"
          label={t('Name')}
          // value={studyName}
          value={trajectory.name}
          onChange={handleChangeName}
          fullWidth
          error={!!errors.name}
          helperText={getErrorMessage(errors.name)}
        />
        <TextField
          id="description"
          label={t('Description')}
          value={studyDescription}
          onChange={handleDescriptionChange}
          fullWidth
          sx={{ mt: 3 }}
        />
      </Box>

      <Typography variant="h2">
        {t('Compute trajectories')}
      </Typography>

      <Typography
        sx={{ my: 3 }}
        variant="h3"
      >
        {t('Load specifications from a previous computation')} {t('(optional)')}
      </Typography>

      <Paper sx={{ mt: 4, mb: 6, width: '100%', bgcolor: 'white' }}>
        <LoadTrajectory
          trajectories={trajectories}
          setTrajectory={setTrajectory}
        />
      </Paper>

      <Typography
        sx={{ my: 3 }}
        variant="h3"
      >
        {t('Specifications of the computation')}
      </Typography>

      <Typography variant="h4" sx={{ display: 'flex', alignItems: 'center' }}>
        {t('Specify starting/arrival dates of forward/backward trajectories')}
        <HelpTooltip>
          Notes :
          <ul>
            <li>Trajectories of air masses can be computed from Sept. 1, 2007, the date from which GDAS-0.5 gridded
              meteorology is available;
            </li>
            <li>Computation for dates after June 12, 2019, are based on GFS-0.25 gridded meteorology;</li>
            <li>Meteorology (either from GDAS or GFS) is not available at a few dates since Sept. 1, 2007, e.g. on June
              12, 2019. Trajectory computation requiring meteorology at these dates will not be performed, the
              corresponding arrival/starting dates will be highlighted in red in the output page of the trajectory
              study.
            </li>
          </ul>
        </HelpTooltip>
      </Typography>
      <Paper sx={{ mt: 1, mb: 5, width: '100%', bgcolor: 'white' }}>
        <DatesSelection
          dates={trajectory.dates}
          setDates={setDates}
        />
      </Paper>

      <Typography variant="h4">
        {t('Specify starting/arrival locations of forward/backward trajectories')}
        {' '}
        ({trajectory.locations.length.toString()})
      </Typography>
      <Paper sx={{ mt: 1, mb: 5, width: '100%', bgcolor: 'white' }}>
        <LocationsSelection
          locations={trajectory.locations}
          setLocations={setLocations}
        />
      </Paper>

      <Typography variant="h4">
        {t('Tune the trajectory model HYSPLIT')}
      </Typography>
      <Paper sx={{ mt: 1, mb: 5, width: '100%', bgcolor: 'white' }}>
        <HysplitConfig
          hour_utc={trajectory.hour_utc}
          runtime={trajectory.runtime}
          top_of_model={trajectory.top_of_model}
          vertical_motion={trajectory.vertical_motion}
          setHysplit={setHysplit}
          errors={errors}
        />
      </Paper>

      <Box sx={{ mb: 3, display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          size="large"
          disabled={isValid()}
          onClick={submit}
        >
          {t('Run')}
        </Button>
      </Box>
    </Container>
  );
};

export default ComputeTrajectory;
