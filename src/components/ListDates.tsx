import React, {MouseEventHandler} from 'react';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Chip, IconButton,
  Paper, Tooltip,
  Typography,
} from '@mui/material';

import {ComputationDate} from '../entities/computation';
import {CheckBox, CheckBoxOutlineBlank} from "@mui/icons-material";

const ListDates = ({
  dates,
  afterTitle = null,
  selectable = false,
  toggle = (value: Date) => {},
  selectAll = () => {},
  deselectAll = () => {},
  ...rest
}: {
  dates: ComputationDate[],
  afterTitle?: React.ReactNode,
  selectable?: boolean,
  toggle?: Function,
  selectAll?: MouseEventHandler,
  deselectAll?: MouseEventHandler,
}) => {
  const { t } = useTranslation();

  return (
    <Box {...rest}>
      <Paper sx={{ px: 3, pt: 3, pb: 2 }}>
        <Typography variant="h4" paragraph>
          {t('List of dates')}

          {Boolean(dates.length) && ` (${dates.length})`}

          {React.isValidElement(afterTitle) && (
            <>
              {' '}
              {afterTitle}
            </>
          )}

          {selectable && (
            <>
              <Tooltip title={'Deselect all'}>
                <IconButton
                  onClick={deselectAll}
                  aria-label="Deselect all"
                  size="small"
                >
                  <CheckBoxOutlineBlank />
                </IconButton>
              </Tooltip>
              <Tooltip title={'Select all'}>
                <IconButton
                  onClick={selectAll}
                  aria-label="Select all"
                  color="info"
                  size="small"
                >
                  <CheckBox />
                </IconButton>
              </Tooltip>
            </>
          )}
        </Typography>

        {dates.map(({
          value,
          selected,
        }) => (
          <Chip
              key={value.toISOString().slice(0, 10)}
              sx={{ mr: 1, mb: 1 }}
              label={value.toISOString().slice(0, 10)}
              color={selectable && selected ? "info" : "default"}
              variant={selectable && selected ? "filled" : "outlined"}
              onClick={selectable ? () => toggle(value) : undefined}
          />
        ))}

        {!dates.length && (
          <Typography sx={{ mr: 1, mb: 1, color: 'text.secondary' }}>
            {t('No date provided')}
          </Typography>
        )}
      </Paper>
    </Box>
  );
};

export default ListDates;
