import React from 'react';
import { Box, Tab, Tabs } from '@mui/material';
import {
  AddCircleOutline as AddIcon,
  List as ListIcon,
  RotateRight as RotateIcon,
  Logout,
} from '@mui/icons-material';
import { Link, useLocation } from 'react-router-dom';
import keycloak from '../keycloak';

const tabPathNames = [
  '/list',
  '/compute-trajectory',
  '/compute-connectivity',
  '/analyse/python',
  '/analyse/r',
  '/help',
  '/credit',
];

const defaultTab = '/list';

const Layout = ({ children }) => {
  const location = useLocation();
  const currentTab = tabPathNames.indexOf(location.pathname) !== -1
    ? location.pathname
    : defaultTab;

  return (
    <Box sx={{ minHeight: '100vh', display: 'flex', flexDirection: 'column' }}>
      <Box
        sx={{
          borderBottom: 1,
          borderColor: 'divider',
          backgroundColor: 'white',
          display: 'flex',
          alignItems: 'flex-end',
          justifyContent: 'space-between',
        }}
      >
        <Box component={Link} to="/">
          <Box
            component="img"
            src="/logo.png"
            alt="Tropolink"
            sx={{
              width: 120,
              height: 56,
              display: 'flex',
              alignItems: 'center',
            }}
          />
        </Box>

        <Tabs value={currentTab} sx={{ minHeight: 0 }}>
          <Tab
            label="List studies"
            value="/list"
            to="/list"
            component={Link}
            sx={{ textTransform: 'none', minHeight: 0 }}
            disableRipple
            icon={<ListIcon />}
            iconPosition="start"
          />
          <Tab
            label="Trajectory"
            value="/compute-trajectory"
            to="/compute-trajectory"
            component={Link}
            sx={{ textTransform: 'none', minHeight: 0 }}
            disableRipple
            icon={<AddIcon />}
            iconPosition="start"
          />
          <Tab
            label="Connectivity"
            value="/compute-connectivity"
            to="/compute-connectivity"
            component={Link}
            sx={{ textTransform: 'none', minHeight: 0 }}
            disableRipple
            icon={<RotateIcon />}
            iconPosition="start"
          />
          <Tab
            label="Python analysis"
            value="/analyse/python"
            to="/analyse/python"
            component={Link}
            sx={{ textTransform: 'none', minHeight: 0 }}
            disableRipple
          />
          <Tab
            label=" R analysis"
            value="/analyse/r"
            to="/analyse/r"
            component={Link}
            sx={{ textTransform: 'none', minHeight: 0 }}
            disableRipple
          />
          <Tab
            label="Help"
            value="/help"
            to="/help"
            component={Link}
            sx={{ textTransform: 'none', minHeight: 0 }}
            disableRipple
          />
          <Tab
            label="Credits"
            value="/credit"
            to="/credit"
            component={Link}
            sx={{ textTransform: 'none', minHeight: 0 }}
            disableRipple
          />
          <Tab
            sx={{ textTransform: 'none', minHeight: 0, minWidth: 0 }}
            icon={<Logout />}
            iconPosition="start"
            disableRipple
            onClick={() => keycloak.logout()}
          />
        </Tabs>
      </Box>
      <Box sx={{ bgcolor: 'grey.100', flex: 1, display: 'flex', flexDirection: 'column' }}>{children}</Box>
    </Box>
  );
};

export default Layout;
