import { Help } from '@mui/icons-material';
import { IconButton, styled, Tooltip, tooltipClasses } from '@mui/material';
import React from 'react';

const HtmlTooltip = styled(({ className, ...props }: any) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 380,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}));

const HelpTooltip = ({ children }) => (
  <HtmlTooltip title={children}>
    <IconButton aria-label="help">
      <Help />
    </IconButton>
  </HtmlTooltip>
);

export default React.memo(HelpTooltip);
