import React, {MouseEventHandler} from 'react';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Chip, IconButton,
  Paper, Tooltip,
  Typography,
} from '@mui/material';

import { ComputationLocation } from '../entities/computation';
import {CheckBox, CheckBoxOutlineBlank} from "@mui/icons-material";

const ListLocations = ({
  locations,
  afterTitle,
  selectable = false,
  toggle = (id: string) => {},
  selectAll = () => {},
  deselectAll = () => {},
  ...rest
}: {
  locations: ComputationLocation[],
  afterTitle?: React.ReactNode,
  selectable?: boolean,
  toggle?: Function,
  selectAll?: MouseEventHandler,
  deselectAll?: MouseEventHandler,
}) => {
  const { t } = useTranslation();

  return (
    <Box {...rest}>
      <Paper sx={{ px: 3, pt: 3, pb: 2 }}>
        <Typography variant="h4" paragraph>
          {t('List of locations')}

          {Boolean(locations.length) && ` (${locations.length})`}

          {React.isValidElement(afterTitle) && (
            <>
              {' '}
              {afterTitle}
            </>
          )}

          {selectable && (
            <>
              <Tooltip title={'Deselect all'}>
                <IconButton
                  onClick={deselectAll}
                  aria-label="Deselect all"
                  size="small"
                >
                  <CheckBoxOutlineBlank />
                </IconButton>
              </Tooltip>
              <Tooltip title={'Select all'}>
                <IconButton
                  onClick={selectAll}
                  aria-label="Select all"
                  color="info"
                  size="small"
                >
                  <CheckBox />
                </IconButton>
              </Tooltip>
            </>
          )}
        </Typography>

        {locations.map(({
          value,
          id,
          selected,
          // valid,
        }) => (
          <Chip
              key={id}
              sx={{ mr: 1, mb: 1 }}
              label={value}
              color={selectable && selected ? "info" : "default"}
              variant={selectable && selected ? "filled" : "outlined"}
              onClick={selectable ? () => toggle(id) : undefined}
          />
        ))}

        {!locations.length && (
          <Typography sx={{ mr: 1, mb: 1, color: 'text.secondary' }}>
            {t('No location provided')}
          </Typography>
        )}
      </Paper>
    </Box>
  );
};

ListLocations.defaultProps = {
  afterTitle: null,
};

export default ListLocations;
