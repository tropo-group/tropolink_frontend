import React, { useEffect } from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import { Box } from '@mui/material';

function getCentroid (arr) {
  return arr.reduce(
    (x, y) => [x[0] + y[0] / arr.length, x[1] + y[1] / arr.length],
    [0, 0],
  );
}

const Map = ({ points }) => {
  const mapRef = React.useRef();
  const [map, setMap] = React.useState<any>(null);

  useEffect(
    () => {
      if (!mapRef.current || map) {
        return;
      }

      const mapInstance = L.map(mapRef.current, {
        center: [0, 0],
        zoom: 4,
        layers: [
          L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution:
              '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
          }),
        ],
      });
      setMap(mapInstance);
    },
    [map],
  );

  useEffect(() => {
    if (!map) {
      return;
    }

    const center = getCentroid(points);

    points.forEach(point => {
      L.circleMarker(point, { radius: 4 }).addTo(map);
    });

    map.setView(center);
  }, [map, points]);

  return <Box id="map" ref={mapRef} style={{ height: '350px' }} />;
};

export default React.memo(Map);
