// @ts-nocheck
import React from 'react';
import { Avatar, Chip, Tooltip } from '@mui/material';

const StatusChip = ({
  mini = false,
  status,
  dim = false,
  sx = {},
  alt = '',
  ...rest
}) => {
  let color = 'default';
  switch (status) {
    case 'succeeded':
      color = 'success';
      break;
    case 'failed':
      color = 'error';
      break;
    default:
      color = 'info';
      break;
  }

  if (mini) {
    return (
      <Avatar
        size="small"
        label={status}
        bgcolor={color}
        sx={{
          bgcolor: `${color}.${dim ? 'light' : 'main'}`,
          width: 24,
          height: 24,
          mr: 0.5,
          fontSize: '0.8rem',
          opacity: dim ? 0.8 : 1,
          ...sx,
        }}
        {...rest}
      >
        <Tooltip title={alt}>
          <span>{dim ? 'C' : 'T'}</span>
        </Tooltip>
      </Avatar>
    );
  }

  return (
    <Chip size="small" label={status} color={color} {...rest} />
  );
};

export default StatusChip;
