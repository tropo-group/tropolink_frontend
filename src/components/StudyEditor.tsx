import React from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material';
import { useTranslation } from 'react-i18next';

const StudyEditor = ({
  study,
  onValidation = () => {},
  onClose = () => {},
}) => {
  const { t } = useTranslation();
  const fieldRef = React.useRef();

  const handleSave = () => {
    // @ts-expect-error
    onValidation({ id: study.id, description: fieldRef?.current?.value });
    onClose();
  };

  return (
    <Dialog open={Boolean(study)} onClose={onClose}>
      {Boolean(study) && (
        <>
          <DialogTitle>{study.name}</DialogTitle>
          <DialogContent sx={{ minWidth: { xs: 0, sm: 500 } }}>
            <TextField
              autoFocus
              inputRef={fieldRef}
              defaultValue={study.description}
              margin="dense"
              id="description"
              label={t('Description')}
              fullWidth
              variant="standard"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose}>{t('Cancel')}</Button>
            <Button onClick={handleSave}>{t('Save')}</Button>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
};

export default StudyEditor;
