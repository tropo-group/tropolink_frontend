import DOMPurify from 'dompurify';
import { marked } from 'marked';
import React, { useEffect, useState } from 'react';

function createMarkup (text: string) {
  const parsedMessage = marked.parse(text);
  const parsedCleanMessage = DOMPurify.sanitize(parsedMessage);

  return { __html: parsedCleanMessage };
}

const MarkdownContent = ({ fileName }: { fileName: string }) => {
  const [content, setContent] = useState('');

  useEffect(() => {
    fetch(`/content/${fileName}`)
      .then(res => res.text())
      .then(res => setContent(res))
      .catch(err => console.error(err));
  });

  return (
    <div // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={createMarkup(content)}
    />
  );
};

export default MarkdownContent;
