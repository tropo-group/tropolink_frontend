// @ts-nocheck
import React from 'react';
import {Box, LinearProgress, Typography} from '@mui/material';

const ConnectivityProgress = ({
  connectivity
}) => {
    if (connectivity.status === 'succeeded') {
        return (
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Box>
                    <Typography variant="body2">Succeeded</Typography>
                </Box>
                <Box sx={{ width: '100%' }}>
                    <LinearProgress variant="determinate" value={100} />
                </Box>
            </Box>
        );
    }

    if (connectivity.status === 'failed') {
        return (
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Box>
                    <Typography variant="body2">Failed</Typography>
                </Box>
                <Box sx={{ width: '100%' }}>
                    <LinearProgress variant="determinate" value={0} />
                </Box>
            </Box>
        );
    }

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Box>
        <Typography variant="body2">{`In progress ${!!connectivity.progress ? connectivity.progress + '%' : '' }`}</Typography>
      </Box>
      <Box sx={{ width: '100%' }}>
        <LinearProgress variant="determinate" value={connectivity.progress} />
      </Box>
    </Box>
  );
};

export default ConnectivityProgress;
