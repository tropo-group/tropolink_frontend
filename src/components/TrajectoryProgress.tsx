// @ts-nocheck
import React from 'react';
import {Box, LinearProgress, Typography} from '@mui/material';

const TrajectoryProgress = ({
  trajectory
}) => {
    if (!trajectory.jobs || !trajectory.jobs_successful || !trajectory.jobs_failed) {
        return (
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Box>
                    <Typography variant="body2">Pending</Typography>
                </Box>
                <Box sx={{ width: '100%' }}>
                    <LinearProgress />
                </Box>
            </Box>
        );
    }

    const successful = trajectory.jobs_successful.length,
        total = Object.keys(trajectory.jobs).length,
        failed = trajectory.jobs_failed.length,
        remaining = total - successful - failed,
        successfulProgress = successful * 100 / total,
        remainingProgress = remaining * 100 / total;

    if (trajectory.status === 'succeeded') {
        return (
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Box>
                    <Typography variant="body2">{`Succeeded ${trajectory.jobs_successful.length} / ${Object.keys(trajectory.jobs).length}${failed ? ` (${failed} failed)`: ''}`}</Typography>
                </Box>
                <Box sx={{ width: '100%' }}>
                    <LinearProgress variant="determinate" value={100} />
                </Box>
            </Box>
        );
    }

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Box>
        <Typography variant="body2">{`${trajectory.jobs_successful.length} / ${Object.keys(trajectory.jobs).length}${failed ? ` (${failed} failed)`: ''}`}</Typography>
      </Box>
      <Box sx={{ width: '100%' }}>
        <LinearProgress variant="buffer" value={successfulProgress} valueBuffer={successfulProgress + remainingProgress} />
      </Box>
    </Box>
  );
};

export default TrajectoryProgress;
