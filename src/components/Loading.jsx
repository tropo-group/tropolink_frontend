import React from 'react';
import { Box, CircularProgress } from '@mui/material';

const Loading = () => (
  <Box sx={{ textAlign: 'center', my: 4 }}>
    <CircularProgress />
  </Box>
);

export default Loading;
