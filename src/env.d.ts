/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_API_HOST: string
  readonly VITE_JUPYTER_HOST: string
  readonly VITE_JUPYTER_NOTEBOOK: string
  readonly VITE_KEYCLOAK_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
