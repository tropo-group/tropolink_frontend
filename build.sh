# Drop modules & ViteJS cache for immutable/idempotent build
rm -rf ./node_modules && \
docker run --rm --tty \
  --workdir /build \
  --volume $(pwd):/build \
  --user $UID:${GID} \
  node:`cat .nvmrc` \
  sh -c "npm --cache /tmp/.npm/ ci && npm run build" && \
echo "Build done successfully"
